﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CountdownTime : MonoBehaviour {

    public int initSeconds = 300;
    public string textFormat = "{0:D2}:{1:D2}:{2:D2}";
    public int warnSeconds = 10;
    public Color warnColor = Color.red;
    public bool launchOnAwake = true;

    [System.Serializable]
    public class FinishEvent : UnityEvent { }
    public FinishEvent onFinish;

    private int seconds;
    private Text text;
    private Color originColor;
    private bool warning = false;

	void Awake() {
        text = GetComponent<Text>();
        originColor = text.color;
        if (launchOnAwake) {
            Launch();
        }
    }

    public void Launch() {
        seconds = initSeconds;
        warning = false;
        text.color = originColor;
        UpdateText();
        EzTiming.CallAtTimes(seconds, 1f, Count, () => {
            if (onFinish != null) {
                onFinish.Invoke();
            }
            iTween.Stop(gameObject);
            EzTiming.KillCoroutines(GetInstanceID().ToString());
            text.color = originColor;
        });
    }

    public void Pause() {
        EzTiming.PauseCoroutines(GetInstanceID().ToString());
    }

    public void Resume() {
        EzTiming.ResumeCoroutines(GetInstanceID().ToString());
    }

    public void Stop() {
        EzTiming.KillCoroutines(GetInstanceID().ToString());
    }

    void Count() {
        if (--seconds < warnSeconds && !warning) {
            warning = true;
            iTween.FadeTo(gameObject, iTween.Hash("color", warnColor, "time", 0.2f,
                "easetype", iTween.EaseType.easeInOutSine, "looptype", iTween.LoopType.pingPong));
        }
        UpdateText();
    }

    void UpdateText() {
        System.TimeSpan ts = new System.TimeSpan(0, 0, Mathf.RoundToInt(seconds));
        if (ts.Hours > 0) {
            text.text = ts.ToString();
        } else {
            text.text = ts.ToString().Substring(3);
        }
    }
}
