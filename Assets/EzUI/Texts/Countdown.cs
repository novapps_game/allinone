﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Countdown : MonoBehaviour {

    [System.Serializable]
    public class FinishEvent : UnityEvent { }

    public int count = 5;
    public int maxTime;
    public float time;
    public float progress { get { return time / maxTime; } }
    public float scaleRate = 1.5f;
    public float fadeDelay = 0.5f;
    public string countSound;
    public bool countOnStart = true;
	public bool destoryOnFinish = false;
    public FinishEvent onFinish;

    private Text text;
    private Vector3 scale;

    void Awake() {
        time = maxTime = count;
        text = GetComponent<Text>();
        scale = new Vector3(scaleRate, scaleRate, 1);
    }

    // Use this for initialization
    void Start() {
        if (countOnStart) {
            Count();
        }
    }
	
	// Update is called once per frame
	void Update() {
        text.text = count.ToString();
    }

    void Count() {
        if (!string.IsNullOrEmpty(countSound)) {
            GameManager.instance.PlaySound(countSound);
        }
        iTween.ScaleTo(gameObject, iTween.Hash("scale", scale, "time", 1,
            "easetype", iTween.EaseType.easeOutCubic, "onupdate", "OnCounting", "oncomplete", "OnCounted"));
        iTween.FadeTo(gameObject, iTween.Hash("alpha", 0, "time", 1 - fadeDelay,
            "delay", fadeDelay));
    }

    void OnCounting() {
        time -= Time.deltaTime;
    }

    void OnCounted() {
        iTween.Stop(gameObject);
        transform.localScale = Vector3.one;
        text.color = text.color.NewA(1);
        if (--count > 0) {
            Count();
		} else {
			if (onFinish != null) {
				onFinish.Invoke();
			}
			if (destoryOnFinish) {
				Destroy(gameObject);
			}
        }
    }

    public void Pause() {
        iTween.Pause(gameObject);
    }

    public void Resume() {
        iTween.Resume(gameObject);
    }
}
