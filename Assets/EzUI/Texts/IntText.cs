﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntText : MonoBehaviour {

    public int targetValue;
    public float changeSpeed = 10;
    public string format = "{0:D}";

    private const float TOL = 1e-3f;

    private Text text;
    private float value;
    private float maxDiff;

	void Awake() {
        text = GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update() {
        float diff = Mathf.Abs(value - targetValue);
        maxDiff = Mathf.Max(maxDiff, diff);
        if (diff < maxDiff * TOL) {
            value = targetValue;
        } else {
            value = Mathf.Lerp(value, targetValue, Time.deltaTime * changeSpeed);
        }
        text.text = string.Format(format, Mathf.RoundToInt(value));
	}

    public void SetValue(int value) {
        targetValue = value;
        this.value = value;
    }
}
