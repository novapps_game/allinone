﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeOfStage : MonoBehaviour {

    public int stageId;
    public string format;
    public bool updatePerFrame = false;

    private Text text;

	// Use this for initialization
	void Start() {
        text = GetComponent<Text>();
        UpdateText();
    }
	
	// Update is called once per frame
	void Update() {
		if (updatePerFrame) {
            UpdateText();
        }
	}

    void UpdateText() {
        float time = GameManager.instance.GetTimeOfStage(stageId);
        text.text = string.Format(format, time);
    }
}
