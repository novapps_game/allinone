﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class ScrollListItem : MonoBehaviour {

    public int index;

    public virtual void Init(int index) {
        this.index = index;
        OnInit();
    }

    protected virtual void OnInit() {
        name = (index + 1).ToString();
    }
}
