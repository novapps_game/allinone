﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour {

    public enum Direction {
        Horizontal,
        Vertical
    }
    public Direction direction = Direction.Horizontal;
    public float maxSize;
    public float minProgress = 0.2f;
    public string progressFormat = "{0:0%}";
    public float updateSpeed = 5f;

    public float progress {
        get {
            return direction == Direction.Vertical
                ? bar.sizeDelta.y / maxSize
                : bar.sizeDelta.x / maxSize;
        }
        set {
            value = Mathf.Max(minProgress, value);
            targetSize = (direction == Direction.Vertical)
                ? targetSize.NewY(value * maxSize)
                : targetSize.NewX(value * maxSize);
        }
    }

    private RectTransform bar;
    private Text text;
    private Vector2 targetSize;

    void Awake() {
        bar = transform.GetComponent<RectTransform>();
        text = transform.GetComponentInChildren<Text>();
        targetSize = bar.sizeDelta;
    }

    void Update() {
        text.text = string.Format(progressFormat, progress);
        bar.sizeDelta = Vector2.Lerp(bar.sizeDelta, targetSize, updateSpeed * Time.deltaTime);
    }
}
