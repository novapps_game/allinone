﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CountdownImage : MonoBehaviour {

    [System.Serializable]
    public class FinishEvent : UnityEvent { }

    public Sprite[] sprites;
    public float scaleRate = 1.5f;
    public float fadeDelay = 0.5f;
    public string countSound;
    public bool destoryOnFinish = false;
    public FinishEvent onFinish;

    private Image image;
    private Vector3 scale;
    private int index;

    // Use this for initialization
    void Start() {
        image = GetComponent<Image>();
        scale = new Vector3(scaleRate, scaleRate, 1);
        if (sprites.Length > 0) {
            Count();
        }
    }

    // Update is called once per frame
    void Update() {
        if (index < sprites.Length) {
            image.sprite = sprites[index];
        }
    }

    void Count() {
        if (!string.IsNullOrEmpty(countSound)) {
            GameManager.instance.PlaySound(countSound);
        }
        iTween.ScaleTo(gameObject, iTween.Hash("scale", scale, "time", 1,
            "easetype", iTween.EaseType.easeOutCubic, "oncomplete", "OnCounted"));
        iTween.FadeTo(gameObject, iTween.Hash("alpha", 0, "time", 1 - fadeDelay,
            "delay", fadeDelay));
    }

    void OnCounted() {
        iTween.Stop(gameObject);
        transform.localScale = Vector3.one;
        image.color = image.color.NewA(1);
        if (++index < sprites.Length) {
            Count();
        } else {
            if (onFinish != null) {
                onFinish.Invoke();
            }
            if (destoryOnFinish) {
                Destroy(gameObject);
            }
        }
    }
}
