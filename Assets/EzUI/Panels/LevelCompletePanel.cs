﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCompletePanel : Panel {

	public void Menu() {
        OnClick("Menu");
        GameManager.instance.SwitchScene("Menu");
    }

    public void Share() {
        OnClick("Share");
        GameManager.instance.ShareApp();
    }

    public void Replay() {
        OnClick("Replay");
        Board.GetInstance().Load();
        Close();
    }

    public void Next() {
        OnClick("Next");
        if (Board.GetInstance().NextLevel()) {
            Close();
        }
    }
}
