﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HRDGameOverPanel : Panel {

    public GameObject newObj;
    public GameObject normalObj;

    public GameObject nextObj;
    public GameObject iapObj;

    public Text levelText;

    protected override void OnStart() {
        base.OnStart();

        nextObj.SetActive(GameManager.instance.hrdHasNextLevel && (
            GameManager.instance.HrdIsLevelUnlocked(GameManager.hrdcurrentLevel + 1) ||
            EzAds.IsRewardedVideoLoaded()));
        iapObj.SetActive(false);

        levelText.text = GameManager.hrdcurrentLevel + "x" + GameManager.hrdcurrentLevel;

        if (Mathf.FloorToInt(HRDGameController.GetInstance().time) <= GameManager.instance.HRDGetScoreOfCurrentLevel()) {
            newObj.SetActive(true);
            normalObj.SetActive(false);
        } else {
            newObj.SetActive(false);
            normalObj.SetActive(true);
        }
    }

    public void NextLevel() {
        if (GameManager.instance.hrdHasNextLevel) {
            if (GameManager.instance.HrdIsLevelUnlocked(GameManager.hrdcurrentLevel + 1)) {
                SelectedLevel();
            } else if (EzAds.IsRewardedVideoLoaded()) {
                Panel.Open("UnlockLevelPanel", null, (action) => {
                    if (action == "OK") {
                        EzAds.ShowRewardedVideo((_) => {
                            GameManager.instance.HrdUnlockLevel(GameManager.hrdcurrentLevel + 1);
                            Panel.Close("UnlockLevelPanel");
                            SelectedLevel();
                        });
                    }
                });
            }
        }
    }

    void SelectedLevel() {
        GameManager.instance.HrdNextLevel();
        GameManager.instance.LoadScene("HRDGame");
    }

    public void OnPruchasSuccess() {
        GameManager.instance.purchased = true;
        iapObj.SetActive(false);
        iapObj.SetActive(true);
    }

}