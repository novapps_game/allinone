﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageClearPanel : Panel {

    private GameObject nextButton;

	protected override void OnAwake() {
        base.OnAwake();
        nextButton = transform.Find("InnerPanel/Next").gameObject;
    }

    protected override void OnUpdate() {
        base.OnUpdate();
        nextButton.SetActive(GameManager.instance.hasNextStage);
    }

    public void Next() {
        OnClick("Next");
        GameManager.instance.NextStage();
    }
}
