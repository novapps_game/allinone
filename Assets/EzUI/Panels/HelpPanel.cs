﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpPanel : Panel {

    public GameObject[] helps;

    private int index;

    // Use this for initialization
    void Start () {
        helps[index].SetActive(true);

        index++;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTouchUp() {
        if(index < helps.Length) {
            helps[index++].SetActive(true); 
        } else {
            Close();
        }
    }

    protected override void OnClose() {
        base.OnClose();

        if(HRDGameController.GetInstance() != null) {
            HRDGameController.GetInstance().CloseHelp();
        }
    }
}
