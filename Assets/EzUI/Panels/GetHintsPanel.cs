﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetHintsPanel : Panel {

	public void GetHints() {
        EzAds.ShowRewardedVideo(OnGetHints);
    }

    void OnGetHints(int amount) {
        //GameManager.instance.hints += amount;
        GainCoins.GetInstance().SpawnCoins(amount);
        Close();
    }
}
