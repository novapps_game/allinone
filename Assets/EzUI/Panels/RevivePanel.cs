﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RevivePanel : Panel {

	public void Revive() {
		OnClick("Revive");
        Open("LoadingPanel");
        transform.Find("InnerPanel/Countdown").gameObject.SetActive(false);
		EzAnalytics.LogEvent("Revive", "Goto");
        EzAds.ShowRewardedVideo(OnVideoRewarded, OnVideoCanceled);
	}

    void OnVideoRewarded(int amount) {
        Close();
        Debug.Log("video rewarded for revive");
        EzAnalytics.LogEvent("Revive", "Succeeded");
        GameController.GetInstance().Revive();
    }

    void OnVideoCanceled() {
        Cancel();
        Debug.Log("video canceled for revive");
        EzAnalytics.LogEvent("Revive", "Failed");
    }

	protected override void OnCancel() {
		base.OnCancel();
        GameManager.instance.GameOver();
	}

    protected override void OnClose() {
        base.OnClose();
        Close("LoadingPanel");
    }
}
