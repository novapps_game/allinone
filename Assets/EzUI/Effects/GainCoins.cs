﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GainCoins : Singleton<GainCoins> {

    public Transform target;

    public float spawnRange = 10f;
    public float spawnInterval = 0.05f;
    public float tweenDelay = 0.1f;
    public float moveTime = 1f;
    public float scaleTime = 0.5f;
    public float minScale = 2f;
    public float maxScale = 4f;
    public float endScale = 0.8f;

    private Transform baseCoin;
    private int coinCount;
    private Vector3 targetScale;

	// Use this for initialization
	void Start() {
        baseCoin = transform.GetChild(0);
        targetScale = Vector3.one * endScale;
    }

    public void SpawnCoins(int count) {
        EzTiming.RunCoroutine(SpawnLoop(count));
    }

    IEnumerator<float> SpawnLoop(int count) {
        coinCount = 0;
        while (coinCount++ < count) {
            yield return EzTiming.WaitForSeconds(spawnInterval);
            SpawnCoin();
        }
    }

    void SpawnCoin() {
        GameObject coin = Instantiate(baseCoin.gameObject);
        coin.SetActive(true);
        coin.transform.SetParent(transform);
        coin.transform.position = baseCoin.position.ToV2().RandomPositionInSector(spawnRange);
        coin.transform.localScale = Vector3.one * Random.Range(minScale, maxScale);
        iTween.MoveTo(coin, iTween.Hash("position", target.position, "time", moveTime, "delay", tweenDelay,
            "easetype", iTween.EaseType.easeOutQuart));
        iTween.ScaleTo(coin, iTween.Hash("scale", targetScale, "time", scaleTime, "delay", tweenDelay,
            "oncomplete", "OnFlyEnd", "oncompletetarget", gameObject, "oncompleteparams", coin));
    }

    void OnFlyEnd(GameObject coin) {
        //GameManager.instance.GainCoins(1, "RewardedVideo");
        ++GameManager.instance.hints;
        iTween.Stop(coin);
        coin.SetActive(false);
        Destroy(coin, 1);
    }
}
