﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RateButton : MonoBehaviour {

	// Use this for initialization
	void Start() {
        GetComponent<Button>().onClick.AddListener(() => {
			EzAnalytics.LogEvent("Rate", GameManager.instance.currentScene, "Click");
			GameManager.instance.GotoRate();
        });
	}
}
