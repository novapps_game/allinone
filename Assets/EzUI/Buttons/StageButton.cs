﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageButton : MonoBehaviour {

    public int stageId = 1;

	// Use this for initialization
	void Start() {
        Button button = GetComponent<Button>();
        button.interactable = GameManager.instance.IsStageUnlocked(stageId);
        button.onClick.AddListener(() => {
            GameManager.instance.LoadStage(stageId);
        });
        transform.Find("ComingSoon").gameObject.SetActive(stageId > GameManager.instance.maxStages);
        transform.Find("Title").GetComponent<Text>().text = string.Format(Localization.GetMultilineText("StageFormat"), stageId);
        Transform timeOfStage = transform.Find("TimeOfStage");
        float time = GameManager.instance.GetTimeOfStage(stageId);
        if (float.IsInfinity(time)) {
            timeOfStage.gameObject.SetActive(false);
        } else {
            timeOfStage.Find("Time").GetComponent<Text>().text = string.Format(Localization.GetMultilineText("TimeFormat"), time);
        }
	}
	
	// Update is called once per frame
	void Update() {
		
	}
}
