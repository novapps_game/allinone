﻿#if UNITY_PURCHASING
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class IAPRestoreButton : MonoBehaviour {

    [System.Serializable]
    public class RestoreSucceededEvent : UnityEvent { }
    public RestoreSucceededEvent onRestoreSucceeded;

    [System.Serializable]
    public class RestoreFailededEvent : UnityEvent { }
    public RestoreFailededEvent onRestoreFailed;

    // Use this for initialization
    void Start() {
        GetComponent<Button>().onClick.AddListener(Restore);
	}
	
	void Restore() {
        GameManager.instance.RestorePurchasedProducts(OnRestoreSucceeded, OnRestoreFailed);
    }

    void OnRestoreSucceeded() {
        if (onRestoreSucceeded != null) {
            onRestoreSucceeded.Invoke();
        }
    }

    void OnRestoreFailed() {
        if (onRestoreFailed != null) {
            onRestoreFailed.Invoke();
        }
    }
}
#endif