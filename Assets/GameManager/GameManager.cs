﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
#if UNITY_PURCHASING
using UnityEngine.Purchasing;
#endif
#if UNITY_ANDROID && !NO_GPGS
using GooglePlayGames.BasicApi;
using GooglePlayGames;
#endif

public class GameManager : MonoBehaviour
#if UNITY_PURCHASING
    , IStoreListener
#endif
    {

    public static GameManager instance;

	public bool debug = true;
    public bool clearPrefs = false;
    public bool testWin = false;

    public SystemLanguage testLanguage = SystemLanguage.Unknown;
	public int testCoins = -1;
    public int testHints = -1;
    public int testStages = 0;

    public string uidParams { get { return "?uid=" + EzIds.deviceID + "&package=" + EzIds.appID; } }
    public string idParams { get { return "?id=" + EzIds.deviceID + "&package=" + EzIds.appID; } }

    void Awake() {
		if (instance == null) {
			DontDestroyOnLoad(gameObject);
			instance = this;
            Initialize();
		} else if (instance != this) {
			Destroy(gameObject);
		}
	}

    #region Initialization

    void Initialize() {
		Debug.unityLogger.logEnabled = debug;
        if (clearPrefs) EzPrefs.DeleteAll();
        Localization.Init();
        EzImageLoader.Init();
        StartCoroutine(EzRestApi.Post(promoRewardMarkUrl));
        StartCoroutine(EzRemoteConfig.Init(OnInitConfig));
        promoCounts = new int[EzRemoteConfig.promoUrls.Length];
#if UNITY_PURCHASING && UNITY_IOS
        InitIAP();
#endif
        InitLayers();
#if UNITY_ANDROID && !NO_GPGS
        InitGooglePlayGameService();
#endif
        InitLevels();
        LoadPrefabs();
        LoadSprites();
        LoadFonts();
        LoadTexts();
        LoadSounds();
        LoadPlayerData();
        Authenticate();
        AddSceneAction("Home", OnBackHome);
        Application.targetFrameRate = 60;
        // Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    public int defaultLayer;
	void InitLayers() {
		defaultLayer = LayerMask.NameToLayer("Default");
	}

#if UNITY_ANDROID && !NO_GPGS
    void InitGooglePlayGameService() {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = debug;
        PlayGamesPlatform.Activate();
    }
#endif

	void OnInitConfig() {
		if (!EzRemoteConfig.noAds) {
            EzAds.rewardAmount = EzRemoteConfig.adRewardAmount;
            EzAds.RequestInterstitial();
			EzAds.RequestRewardedVideo();
		}
		promoCounts = new int[EzRemoteConfig.promoUrls.Length];
		StartCoroutine(EzImageLoader.CacheImages(EzRemoteConfig.promoImages));
		StartCoroutine(EzImageLoader.CacheImage(EzRemoteConfig.shareAppImage));
		StartCoroutine(EzImageLoader.CacheImage(EzRemoteConfig.shareScoreImage));

		if (EzRemoteConfig.online && currentScene == "Home") {
			OpenUpgrade();
		}
        CheckShareResult();
    }

#endregion

#region Application

    void OnApplicationFocus(bool hasFocus) {
        Debug.Log("OnApplicationFocus: " + hasFocus);
        if (!hasFocus) {
            OnPause();
        }
    }

    void OnApplicationPause(bool pauseStatus) {
        Debug.Log("OnApplicationPause: " + pauseStatus);
        if (!pauseStatus) {
            OnResume();
        }
    }

    public bool paused = false;
    public System.DateTime pauseTime;
    public System.DateTime resumeTime;

    void OnResume() {
        paused = false;
        UpdateRateStatus();
        CheckPromoReward();
        StartCoroutine(EzTime.FetchNetTime((dt) => {
            resumeTime = dt;
            Debug.Log("game resumed at :" + resumeTime);
        }));
    }

    void OnPause() {
        paused = true;
        UpdateRateStatus();
        StartCoroutine(EzTime.FetchNetTime((dt) => {
            pauseTime = dt;
            Debug.Log("game paused at :" + pauseTime);
        }));
    }

#endregion

#region IAP

#if UNITY_PURCHASING
    private ProductCatalog catalog;
	private ProductCatalogItem[] products;
    private IStoreController controller;
    private IExtensionProvider extensions;
    private class PurchaseEvent {
        public System.Action<Product> onPurchaseCompleted;
        public System.Action<Product, PurchaseFailureReason> onPurchaseFailed;
        public PurchaseEvent(System.Action<Product> onPurchaseCompleted, 
            System.Action<Product, PurchaseFailureReason> onPurchaseFailed) {
            this.onPurchaseCompleted = onPurchaseCompleted;
            this.onPurchaseFailed = onPurchaseFailed;
        }
    }
    private PurchaseEvent[] purchaseEvents;
    private Dictionary<string, int> productIdToIndex;
    private System.Action onRestoreSucceeded;
    private System.Action onRestoreFailed;

    void InitIAP() {
        catalog = ProductCatalog.LoadDefaultCatalog();
        products = new ProductCatalogItem[catalog.allProducts.Count];
        catalog.allProducts.CopyTo(products, 0);
        productIdToIndex = new Dictionary<string, int>();
        for (int i = 0; i < products.Length; ++i) {
            productIdToIndex.Add(products[i].id, i);
        }
        purchaseEvents = new PurchaseEvent[products.Length];
        StandardPurchasingModule module = StandardPurchasingModule.Instance();
        module.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;
        ConfigurationBuilder builder = ConfigurationBuilder.Instance(module);
        foreach (var product in catalog.allProducts) {
            if (product.allStoreIDs.Count > 0) {
                var ids = new IDs();
                foreach (var storeID in product.allStoreIDs) {
                    ids.Add(storeID.id, storeID.store);
                }
                builder.AddProduct(product.id, product.type, ids);
            } else {
                builder.AddProduct(product.id, product.type);
            }
        }
        UnityPurchasing.Initialize(this, builder);
    }

    public bool HasProductInCatalog(string productId) {
        foreach (var product in catalog.allProducts) {
            if (product.id == productId) {
                return true;
            }
        }
        return false;
    }

    public void PurchaseProduct(int productIndex,
        System.Action<Product> onPurchaseCompleted = null,
        System.Action<Product, PurchaseFailureReason> onPurchaseFailed = null) {
        if (controller == null) {
            Debug.LogError("Purchase failed because Purchasing was not initialized correctly");
            return;
        }
        string productId = GetProductId(productIndex);
        if (string.IsNullOrEmpty(productId)) {
            Debug.LogError("Product id is empty!");
            return;
        }
        Panel.Open("LoadingPanel");
        purchaseEvents[productIndex] = new PurchaseEvent(onPurchaseCompleted, onPurchaseFailed);
        controller.InitiatePurchase(productId);
    }

    public void RestorePurchasedProducts(System.Action onRestoreSucceeded = null, System.Action onRestoreFailed = null) {
        this.onRestoreSucceeded = onRestoreSucceeded;
        this.onRestoreFailed = onRestoreFailed;
        if (Application.platform == RuntimePlatform.WSAPlayerX86 || Application.platform == RuntimePlatform.WSAPlayerX64 || Application.platform == RuntimePlatform.WSAPlayerARM) {
            Panel.Open("LoadingPanel");
            extensions.GetExtension<IMicrosoftExtensions>().RestoreTransactions();
        } else if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.tvOS) {
            Panel.Open("LoadingPanel");
            extensions.GetExtension<IAppleExtensions>().RestoreTransactions(OnTransactionsRestored);
        } else if (Application.platform == RuntimePlatform.Android && StandardPurchasingModule.Instance().androidStore == AndroidStore.SamsungApps) {
            Panel.Open("LoadingPanel");
            extensions.GetExtension<ISamsungAppsExtensions>().RestoreTransactions(OnTransactionsRestored);
        } else if (Application.platform == RuntimePlatform.Android && StandardPurchasingModule.Instance().androidStore == AndroidStore.CloudMoolah) {
            Panel.Open("LoadingPanel");
            extensions.GetExtension<IMoolahExtension>().RestoreTransactionID((restoreTransactionIDState) => {
                OnTransactionsRestored(restoreTransactionIDState != RestoreTransactionIDState.RestoreFailed && restoreTransactionIDState != RestoreTransactionIDState.NotKnown);
            });
        } else {
            Debug.LogWarning(Application.platform.ToString() + " is not a supported platform for the Codeless IAP restore button");
        }
    }

    public string GetProductId(int index) {
        if (index >= 0 && index < products.Length) {
            return products[index].id;
        }
        return "";
    }

    public int GetProductIndex(string productId) {
        if (productIdToIndex.ContainsKey(productId)) {
            return productIdToIndex[productId];
        }
        Debug.Log("Invalid product id: " + productId);
        return -1;
    }

    public Product GetProduct(string productId) {
        if (controller != null) {
            return controller.products.WithID(productId);
        }
        return null;
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions) {
        Debug.Log("Purchasing initialized.");
        this.controller = controller;
        this.extensions = extensions;
    }

    public void OnInitializeFailed(InitializationFailureReason error) {
        Debug.LogError(string.Format("Purchasing failed to initialize. Reason: {0}", error.ToString()));
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e) {
        OnPurchaseCompleted(e.purchasedProduct);
        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason reason) {
        Panel.Close("LoadingPanel");
        string productId = product.definition.id;
        Debug.Log(string.Format("OnPurchaseFailed(Product {0}, PurchaseFailureReason {1})", productId, reason));
        EzAnalytics.LogEvent("Purchase", productId, reason.ToString());
        int index = GetProductIndex(productId);
        if (purchaseEvents[index] != null && purchaseEvents[index].onPurchaseFailed != null) {
            purchaseEvents[index].onPurchaseFailed(product, reason);
            purchaseEvents[index] = null;
        }
    }

    public void OnPurchaseCompleted(Product product) {
        Panel.Close("LoadingPanel");
        string productId = product.definition.id;
        Debug.Log(string.Format("OnPurchaseCompleted(Product {0})", productId));
        EzAnalytics.LogEvent("Purchase", productId, "Succeeded");
        int index = GetProductIndex(productId);
        OnProductPurchased(index);
        if (purchaseEvents[index] != null && purchaseEvents[index].onPurchaseCompleted != null) {
            purchaseEvents[index].onPurchaseCompleted(product);
            purchaseEvents[index] = null;
        }
    }

    void OnTransactionsRestored(bool success) {
        EzAnalytics.LogEvent("Purchase", "Restore", success ? "Succeeded" : "Failed");
        Debug.Log("Transactions restored: " + success);
        Panel.Close("LoadingPanel");
        if (success) {
            if (onRestoreSucceeded != null) {
                onRestoreSucceeded.Invoke();
                onRestoreSucceeded = null;
            }
        } else {
            if (onRestoreFailed != null) {
                onRestoreFailed.Invoke();
                onRestoreFailed = null;
            }
        }
    }

    void OnProductPurchased(int index) {
        
    }
#endif

#endregion

#region Resource

    public Dictionary<string, GameObject> prefabs;
	void LoadPrefabs() {
		prefabs = new Dictionary<string, GameObject>();
		foreach (GameObject prefab in Resources.LoadAll<GameObject>("Prefabs")) {
			prefabs.Add(prefab.name, prefab);
		}
	}

    public GameObject Instantiate(string prefabName) {
        if (prefabs.ContainsKey(prefabName)) {
            GameObject prefab = prefabs[prefabName];
            GameObject go = Instantiate(prefab);
            go.name = prefab.name;
            return go;
        }
        return null;
    }

    public T Instantiate<T>(string prefabName) where T : Component {
        if (prefabs.ContainsKey(prefabName)) {
            GameObject prefab = prefabs[prefabName];
            GameObject go = Instantiate(prefab);
            go.name = prefab.name;
            return go.GetComponent<T>();
        }
        return default(T);
    }

	public Dictionary<string, Sprite> sprites;
	void LoadSprites() {
		sprites = new Dictionary<string, Sprite>();
		foreach (Sprite sprite in Resources.LoadAll<Sprite>("Sprites")) {
            if (sprites.ContainsKey(sprite.name)) {
                Debug.LogWarning("Duplicated sprite: " + sprite.name);
                continue;
            }
			sprites.Add(sprite.name, sprite);
		}
    }

    [System.Serializable]
    public struct LocalFont {
        public string language;
        public Font font;
    }

    public LocalFont[] fonts;
    public Dictionary<string, Font> localFonts;
    public Font localFont {
        get {
            string language = Localization.GetLanguage();
            if (localFonts.ContainsKey(language)) {
                return localFonts[language];
            }
            return null;
        }
    }
    void LoadFonts() {
        localFonts = new Dictionary<string, Font>();
        foreach (LocalFont font in fonts) {
            localFonts.Add(font.language, font.font);
        }
    }

	public string[] nicknames;
    public int randomNameIndex { get { return Random.Range(0, nicknames.Length); } }
    public string randomNickname { get { return GetNickname(randomNameIndex); } }
    public string GetNickname(int index) {
        return nicknames[index % nicknames.Length];
    }
	void LoadTexts() {
		TextAsset ta = Resources.Load<TextAsset>("Texts/nickname");
		if (ta != null) {
			nicknames = ta.text.Split('\n');
		}
	}

#endregion

#region Audio

    public float soundInterval = 0.05f;
    private AudioSource musicSource;
    private AudioSource soundSource;
    private Dictionary<string, AudioClip> sounds;
    private Dictionary<AudioClip, float> soundTimes;

    public bool muteMusic {
        get { return musicSource.mute; }
        set { musicSource.mute = value; }
    }

    public bool muteSound {
        get { return soundSource.mute; }
        set { soundSource.mute = value; }
    }

    void LoadSounds() {
        AudioSource[] audioSources = GetComponents<AudioSource>();
        musicSource = soundSource = audioSources[0];
        if (audioSources.Length > 1) soundSource = audioSources[1];
        AudioClip[] clips = Resources.LoadAll<AudioClip>("Sounds");
		sounds = new Dictionary<string, AudioClip>();
		soundTimes = new Dictionary<AudioClip, float>();
		foreach (AudioClip clip in clips) {
			sounds.Add(clip.name, clip);
			soundTimes.Add(clip, 0);
		}
	}

	public void PlaySound(string name, float volumeScale = 1.0f) {
		PlaySound(sounds[name], volumeScale);
	}

	public void PlaySound(AudioClip clip, float volumeScale) {
		if (Time.time - soundTimes[clip] > soundInterval) {
			soundTimes[clip] = Time.time;
			soundSource.PlayOneShot(clip, volumeScale);
		}
	}

    public void PlaySound(AudioSource source, string name, float volumeScale = 1.0f) {
        PlaySound(sounds[name], volumeScale);
    }

    public void PlaySound(AudioSource source, AudioClip clip, float volumeScale) {
        if (Time.time - soundTimes[clip] > soundInterval) {
            soundTimes[clip] = Time.time;
            source.PlayOneShot(clip, volumeScale);
        }
    }

    public void StopPlaySound() {
        if (soundSource.isPlaying) {
            soundSource.Stop();
        }
    }

#endregion

#region Scene

    public string currentScene { get { return SceneManager.GetActiveScene().name; } }
	public string targetScene;
    private bool loadingScene = false;

	public void LoadTargetScene(bool useLoadingScreen = false) {
		LoadScene(targetScene, useLoadingScreen);
	}

    public void SwitchScene(string name, bool useLoadingScreen = false) {
        if (currentScene != name) {
            LoadScene(name, useLoadingScreen);
        }
    }

	public void LoadScene(string name, bool useLoadingScreen = false) {
        if (!loadingScene) {
            if (useLoadingScreen) {
                StartCoroutine(LoadSceneAsync(name));
            } else {
                SceneManager.LoadScene(name);
            }
        }
	}

    public void ReloadScene(bool useLoadingScreen = false) {
        LoadScene(currentScene, useLoadingScreen);
    }

	IEnumerator LoadSceneAsync(string name) {
        loadingScene = true;
        EzTiming.KillCoroutines();
        Panel loadingScreen = Panel.Open("LoadingScreen");
        ProgressBar progressBar = null;
        if (loadingScreen != null) {
            progressBar = loadingScreen.GetComponentInChildren<ProgressBar>();
        }
        AsyncOperation async = SceneManager.LoadSceneAsync(name);
        async.allowSceneActivation = false;
        while (async.progress < 0.9f) {
            if (progressBar != null) {
                progressBar.progress = async.progress;
            }
            yield return null;
        }
        if (progressBar != null) {
            progressBar.progress = 1;
        }
        yield return new WaitForSeconds(1f);
        EzAnalytics.LogScreen(name);
		OnLoadScene(name);
        loadingScene = false;
        async.allowSceneActivation = true;
	}

	private Dictionary<string, System.Action> sceneActions = new Dictionary<string, System.Action>();

	public void AddSceneAction(string name, System.Action action) {
		if (!sceneActions.ContainsKey(name)) {
			sceneActions.Add(name, action);
		}
	}

	private void OnLoadScene(string name) {
		if (sceneActions.ContainsKey(name)) {
			sceneActions[name].Invoke();
		}
	}

#endregion

#region Authorication

    public void Authenticate(System.Action<bool> onFinish = null) {
#if SOCIAL && !UNITY_EDITOR
        Social.localUser.Authenticate(success => {
            if (onFinish != null) {
                onFinish.Invoke(success);
            }
        });
#endif

    }

    #endregion

    #region Achievement

    public static void ShowAchievements() {
        if (Social.localUser.authenticated) {
            Social.ShowAchievementsUI();
        }
    }

    #endregion

    #region Leaderboard

#if UNITY_ANDROID && !NO_GPGS
    public const string leaderboard_score3 = "CgkI_YeLlJ0FEAIQAA"; 
    public const string leaderboard_score4 = "CgkI_YeLlJ0FEAIQAw"; 
    public const string leaderboard_score5 = "CgkI_YeLlJ0FEAIQBA"; 
    public const string leaderboard_score6 = "CgkI_YeLlJ0FEAIQBQ"; 
    public const string leaderboard_score7 = "CgkI_YeLlJ0FEAIQBg"; 
    public const string leaderboard_score8 = "CgkI_YeLlJ0FEAIQBw"; 
    public const string leaderboard_score9 = "CgkI_YeLlJ0FEAIQCA"; 
    public const string leaderboard_score10 = "CgkI_YeLlJ0FEAIQCQ"; 
    public const string leaderboard_score11 = "CgkI_YeLlJ0FEAIQCg";
#else
    public const string leaderboard_score = "leaderboard_time";
#endif

    static string GetLeaderboardId() {
#if UNITY_ANDROID && !NO_GPGS
        switch(hrdcurrentLevel) {
            case 3:
                return leaderboard_score3;
            case 4:
                return leaderboard_score4;
            case 5:
                return leaderboard_score5;
            case 6:
                return leaderboard_score6;
            case 7:
                return leaderboard_score7;
            case 8:
                return leaderboard_score8;
            case 9:
                return leaderboard_score9;
            case 10:
                return leaderboard_score10;
            case 11:
                return leaderboard_score11;
        }
        return leaderboard_score3;
#else
        return leaderboard_score + hrdcurrentLevel;
#endif
    }

    public static void ShowLeaderboards() {
        if (Social.localUser.authenticated) {
            Social.ShowLeaderboardUI();
        }
    }

    public static void ReportScore(int score) {
        if (Social.localUser.authenticated) {
            Social.ReportScore(score, GetLeaderboardId(), (success) => {
                Debug.Log("Report score " + (success ? "succeeded" : "failed"));
            });
        }
    }

#endregion

#region Player Data

    private const string PLAY_TIMES = "PLAY_TIMES";
    private const string PLAYER_NAME = "PLAYER_NAME";
	private const string COINS = "COINS";
	private const string SCORE = "SCORE";
	private const string BEST_SCORE = "BEST_SCORE";
    private const string UNLOCKED_STAGES = "UNLOCKED_STAGES";
    private const string UNLOCKED_LEVELS = "UNLOCKED_LEVELS";
    private const string SCORE_OF_STAGE = "SCORE_OF_STAGE";
    private const string STAR_OF_STAGE = "STAR_OF_STAGE";
    private const string TIME_OF_STAGE = "TIME_OF_STAGE";
    private const string VIBRATE = "VIBRATE";
    private const string NO_ADS = "NO_ADS";
    private const string LEVEL_PREFIX = "LEVEL_";
    private const string HINTS = "HINTS";
    private const string PURCHASED= "PURCHASED";
    public bool noAds {
        get { return EzPrefs.GetBool(NO_ADS); }
        set { EzPrefs.SetBool(NO_ADS, value); }
    }
    public int hints {
        get { return testHints > 0 ? testHints : EzPrefs.GetInt(HINTS, 3); }
        set { EzPrefs.SetInt(HINTS, value); }
    }
    public int playTimes;
    public string playerName;
	public Sprite playerAvatar;
    public int coins;
    public int score;
    public int bestScore;
    public int unlockedStages;

    public bool purchased {
        get {
            return EzPrefs.GetBool(PURCHASED, false);
        }
        set {
            EzPrefs.SetBool(PURCHASED, value);
        }
    }

    void LoadPlayerData() {
        playTimes = EzPrefs.GetInt(PLAY_TIMES, 0);
        EzPrefs.SetInt(PLAY_TIMES, ++playTimes);
        playerName = EzPrefs.GetString(PLAYER_NAME, playerName);
        coins = testCoins > 0 ? testCoins : EzPrefs.GetInt(COINS, 0);
        score = EzPrefs.GetInt(SCORE, 0);
        bestScore = EzPrefs.GetInt(BEST_SCORE, 0);
        unlockedStages = testStages > 0 ? testStages : EzPrefs.GetInt(UNLOCKED_STAGES, 1);

        hrdunlockedLevels = EzPrefs.GetInt(UNLOCKED_LEVELS, 4);
        EzPrefs.SetBool("HRD_3", true);
        EzPrefs.SetBool("HRD_4", true);
        if (hrdunlockedLevels > 4) {
            for (int i = 5; i <= hrdunlockedLevels; ++i) {
                EzPrefs.SetBool("HRD_" + i, true);
            }
            EzPrefs.SetInt(UNLOCKED_LEVELS, 0);
        }

        if(playTimes > 1 && HrdIsLevelUnlocked(5)) {
            purchased = true;
        }
    }

    public void FixupPlayerName() {
        if (string.IsNullOrEmpty(playerName)) {
            UpdatePlayerName(Localization.GetText("Guest") + Random.Range(123456789, 987654321));
        }
    }

    public void UpdatePlayerName(string name) {
		playerName = name;
		EzPrefs.SetString(PLAYER_NAME, playerName);
    }

	public void GainCoins(int gains, string from = "") {
		coins += gains;
		EzPrefs.SetInt(COINS, coins);
		EzAnalytics.LogEvent("Coin", "Gain", from, gains);
	}

	public int CostCoins(int costs, string to = "") {
		if (costs > coins) return -1;
		coins -= costs;
		EzPrefs.SetInt(COINS, coins);
		EzAnalytics.LogEvent("Coin", "Cost", to, costs);
		return coins;
	}

	public void GainScore(int gains) {
		UpdateScore(score + gains);
	}

    public bool UpdateScore(int score) {
        this.score = score;
        EzPrefs.SetInt(SCORE, score);
        if (score > bestScore) {
            bestScore = score;
            EzPrefs.SetInt(BEST_SCORE, bestScore);
            return true;
        }
        return false;
    }

    public bool UnlockStage() {
        if (unlockedStages < maxStages) {
            ++unlockedStages;
            EzPrefs.SetInt(UNLOCKED_STAGES, unlockedStages);
            return true;
        }
        return false;
    }

    public int GetScoreOfCurrentStage() {
        return GetScoreOfStage(currentStage);
    }

    public void UpdateScoreOfCurrentStage(int score) {
        UpdateScoreOfStage(currentStage, score);
    }

    public int GetScoreOfStage(int stageId) {
        return EzPrefs.GetInt(SCORE_OF_STAGE + "_" + stageId);
    }

    public void UpdateScoreOfStage(int stageId, int score) {
        string key = SCORE_OF_STAGE + "_" + stageId;
        if (score > EzPrefs.GetInt(key)) {
            EzPrefs.SetInt(key, score);
        }
    }

    public int GetStarOfCurrentStage() {
        return GetStarOfStage(currentStage);
    }

    public void UpdateStarOfCurrentStage(int star) {
        UpdateStarOfStage(currentStage, star);
    }

    public int GetStarOfStage(int stageId) {
        return EzPrefs.GetInt(STAR_OF_STAGE + "_" + stageId);
    }

    public void UpdateStarOfStage(int stageId, int star) {
        string key = STAR_OF_STAGE + "_" + stageId;
        if (star > EzPrefs.GetInt(key)) {
            EzPrefs.SetInt(key, star);
        }
    }

    public float GetTimeOfCurrentStage() {
        return GetTimeOfStage(currentStage);
    }

    public void UpdateTimeOfCurrentStage(float time) {
        UpdateTimeOfStage(currentStage, time);
    }

    public float GetTimeOfStage(int stageId) {
        return EzPrefs.GetFloat(TIME_OF_STAGE + "_" + stageId, float.PositiveInfinity);
    }

    public void UpdateTimeOfStage(int stageId, float time) {
        string key = TIME_OF_STAGE + "_" + stageId;
        if (time < EzPrefs.GetFloat(key)) {
            EzPrefs.SetFloat(key, time);
        }
    }

    public int GetLevelStars(string level) {
        return EzPrefs.GetInt(LEVEL_PREFIX + level);
    }

    public void SetLevelStars(string level, int stars) {
        if (stars > GetLevelStars(level)) {
            EzPrefs.SetInt(LEVEL_PREFIX + level, stars);
        }
    }

    public void SetCurrentLevelStars(int stars) {
        SetLevelStars(currentLevel, stars);
    }

	public bool vibrateEnabled {
		get { return EzPrefs.GetInt(VIBRATE, 1) != 0; }
		set { EzPrefs.SetInt(VIBRATE, value ? 1 : 0); }
	}

	public void Vibrate() {
#if UNITY_ANDROID || UNITY_IOS
        if (vibrateEnabled) Handheld.Vibrate();
#endif
	}

#endregion

#region Game State

    public int backHomeTimes = 0;
    public void OnBackHome() {
		if (EzRemoteConfig.online && backHomeTimes++ % EzRemoteConfig.promoInterval == 0) {
			for (; promoIndex < promoCounts.Length; ++promoIndex) {
				if (!string.IsNullOrEmpty(promoUrl) && 
                    !EzIds.bundleID.Equals(promoId) && 
                    promoCount < EzRemoteConfig.promoMaxCount) {
					++promoCount;
					OpenPromo();
					break;
				}
			}
		}
	}
    private int reviveCount = 0;
    private float gameStartTime;
    public void OnGameStart() {
        reviveCount = 0;
        EzAnalytics.LogEvent("Game", "Start", playerName);
        gameStartTime = Time.realtimeSinceStartup;
    }

    public void OnGameOver(bool revivable = false) {
        EzAnalytics.LogEvent("Game", "Over", score.ToString());
        if (revivable && CheckRevive()) return;
        GameOver();
    }

    public int gameOverTimes = 0;
    public void GameOver() {
        ++gameOverTimes;
        if (!CheckRate()) {
            CheckAds();
            if (HRDGameController.GetInstance() != null) {
                OpenGameOver();
            }
        }
    }

    public void OpenGameOver() {
        Panel.Open("GameOverPanel");
    }

    public bool CheckRevive() {
        if (Time.realtimeSinceStartup - gameStartTime > EzRemoteConfig.reviveMinTime &&
            (EzRemoteConfig.reviveMaxCount == 0 || reviveCount++ < EzRemoteConfig.reviveMaxCount) &&
            EzAds.IsRewardedVideoLoaded()) {
            OpenRevive();
            return true;
        }
        if (!EzAds.IsRewardedVideoLoaded()) {
            EzAds.RequestRewardedVideo();
        }
        return false;
    }

    public void OpenRevive() {
        Panel.Open("RevivePanel");
    }

    public static void RequestAds() {
        if (!EzRemoteConfig.noAds) {
            EzAds.RequestInterstitial();
            EzAds.RequestRewardedVideo();
        }
    }

    public bool CheckAds(System.Action next = null) {
        if (!noAds && !EzRemoteConfig.noAds && EzAds.IsInterstitialLoaded() &&
            (Time.realtimeSinceStartup - gameStartTime > EzRemoteConfig.adShowTimeInterval ||
                (gameOverTimes >= EzRemoteConfig.adShowFirst &&
                    (gameOverTimes - EzRemoteConfig.adShowFirst) % EzRemoteConfig.adShowInterval == 0))) {
            gameStartTime = Time.realtimeSinceStartup;
            EzAds.ShowInterstitial(next);
            return true;
        }
        if (!noAds && !EzRemoteConfig.noAds && !EzAds.IsInterstitialLoaded()) {
            EzAds.RequestInterstitial();
        }
        if (next != null) {
            next.Invoke();
        }
        return false;
    }

#endregion

#region Stage

    public int maxStages = 5;
    public int currentStage = 1;
    public bool hasNextStage { get { return currentStage < maxStages; } }

    public bool IsStageUnlocked(int stageId) {
        return stageId <= unlockedStages;
    }

    public bool NextStage() {
        return LoadStage(currentStage + 1);
    }

    public bool LoadStage(int stageId) {
        if (IsStageUnlocked(stageId)) {
            currentStage = stageId;
            LoadScene("Stage" + currentStage.ToString("D2"));
            return true;
        }
        return false;
    }

    public void OnStageStart() {
        EzAnalytics.LogEvent("Stage" + currentStage, "Start");
    }

    public void OnStageClear() {
        if (currentStage == unlockedStages) {
            UnlockStage();
        }
        CheckAds();
        OpenStageClear();
    }

    public void OpenStageClear() {
        EzAnalytics.LogEvent("Stage" + currentStage, "Clear", score.ToString());
        Panel.Open("StageClearPanel");
    }

#endregion

#region Rate

    private enum JumpStatus {
        Resumed,
        Jumping,
        Paused,
    }

    private JumpStatus rateStatus = JumpStatus.Resumed;
    private float rateTime;
    private int rateCount;

    public bool rated {
		get { return EzPrefs.GetInt(Application.version, 0) != 0; }
		set { EzPrefs.SetInt(Application.version, value ? 1 : 0); }
	}

    private System.Action onRateSucceeded;
    private System.Action onRateFailed;

    public bool CheckRate() {
        if (!rated && EzRemoteConfig.online && !string.IsNullOrEmpty(EzRemoteConfig.rateUrl) &&
            rateCount < EzRemoteConfig.rateMaxCount &&
            gameOverTimes >= EzRemoteConfig.rateFirst &&
            (gameOverTimes - EzRemoteConfig.rateFirst) % EzRemoteConfig.rateInterval == 0) {
            ++rateCount;
            OpenRate();
            return true;
        }
        return false;
    }

    public void OpenRate() {
        Panel.Open("RatePanel");
    }

	public void GotoRate(System.Action onRateSucceeded = null, System.Action onRateFailed = null) {
		if (string.IsNullOrEmpty(EzRemoteConfig.rateUrl)) return;
        this.onRateSucceeded = onRateSucceeded;
        this.onRateFailed = onRateFailed;
        rateStatus = JumpStatus.Jumping;
		EzAnalytics.LogEvent("Rate", "Goto");
		Application.OpenURL(EzRemoteConfig.rateUrl);
	}

	void UpdateRateStatus() {
		if (paused && rateStatus == JumpStatus.Jumping) {
			rateStatus = JumpStatus.Paused;
			rateTime = Time.realtimeSinceStartup;
		} else if (!paused && rateStatus == JumpStatus.Paused) {
			rateStatus = JumpStatus.Resumed;
			rateTime = Time.realtimeSinceStartup - rateTime;
			if (rateTime < EzRemoteConfig.rateMinTime) {
				OnRateFailed();
			} else {
				OnRateSucceeded();
			}
		}
	}

	void OnRateSucceeded() {
		rated = true;
		EzAnalytics.LogEvent("Rate", "Succeeded");
        if (onRateSucceeded != null) {
            onRateSucceeded.Invoke();
        }
    }

	void OnRateFailed() {
		EzAnalytics.LogEvent("Rate", "Failed");
        if (onRateFailed != null) {
            onRateFailed.Invoke();
        }
	}

#endregion

#region Promo

    [System.Serializable]
    public class PromoRewardResult {
        public bool complete;
    }

    [System.Serializable]
    public class PromoRewardData {
        public int code;
        public string msg;
        public PromoRewardResult res;
    }

    private int promoIndex;

    public string promoId { get { return EzRemoteConfig.promoIds[promoIndex]; } }
    public string promoUrl { get { return EzRemoteConfig.promoUrls[promoIndex]; } }
    public string promoImage { get { return EzRemoteConfig.promoImages[promoIndex]; } }

    private int[] promoCounts;
    public int promoCount {
        get { return promoCounts[promoIndex]; }
        set { promoCounts[promoIndex] = value; }
    }

    private const string promoApiUrl = "http://share.game.adesk.com/excitation";
    public string promoRewardUrl { get { return promoApiUrl + uidParams; } }
    public string promoRewardMarkUrl { get { return promoApiUrl + uidParams + "&type=setComplete"; } }

    public void OpenPromo() {
		Panel.Open("PromoPanel");
	}

    public void CheckPromoReward() {
        for (int i = 0; i < EzRemoteConfig.promoIds.Length; ++i) {
            string targetId = EzRemoteConfig.promoIds[i];
            if (string.IsNullOrEmpty(targetId)) continue;
            if (EzPrefs.GetBool(targetId)) continue;
            StartCoroutine(EzRestApi.Get(promoRewardUrl + "&topackage=" + EzCrypto.MD5(targetId), (json) => {
                PromoRewardData data = JsonUtility.FromJson<PromoRewardData>(json);
                if (data.code == 0 && data.res.complete) {
                    OnPromoRewardSucceeded(targetId);
                } else {
                    OnPromoRewardFailed(targetId);
                }
            }));
        }  
    }

    void OnPromoRewardSucceeded(string targetId) {
        EzPrefs.SetBool(targetId, true);
        EzAnalytics.LogEvent("PromoReward", targetId, "Succeeded");
    }

    void OnPromoRewardFailed(string targetId) {
        EzAnalytics.LogEvent("PromoReward", targetId, "Failed");
    }

    public void GotoPromo() {
		if (string.IsNullOrEmpty(promoUrl)) return;
		EzAnalytics.LogEvent("Promo", "Goto", promoId);
		Application.OpenURL(promoUrl);
	}

    public void GotoPromoReward(int index) {
        if (index >= EzRemoteConfig.promoIds.Length) {
            Debug.LogWarning("Invalid promo index: " + index);
            return;
        }
        string targetId = EzRemoteConfig.promoIds[index];
        string targetUrl = EzRemoteConfig.promoUrls[index];
        if (string.IsNullOrEmpty(targetId) || string.IsNullOrEmpty(targetUrl)) return;
        StartCoroutine(EzRestApi.Post(promoRewardUrl + "&topackage=" + EzCrypto.MD5(targetId), (json) => {
            PromoRewardData data = JsonUtility.FromJson<PromoRewardData>(json);
            if (data.code == 0 && data.res.complete) {
                OnPromoRewardSucceeded(targetId);
            } else {
                EzAnalytics.LogEvent("PromoReward", targetId, "Goto");
                Application.OpenURL(targetUrl);
            }
        }));
    }

#endregion

#region Upgrade

    public void OpenUpgrade() {
		if (string.IsNullOrEmpty(EzRemoteConfig.upgradeUrl)) return;
		Panel.Open("UpgradePanel");
	}

	public void GotoUpgrade() {
		if (string.IsNullOrEmpty(EzRemoteConfig.upgradeUrl)) return;
		EzAnalytics.LogEvent("Upgrade", "Goto");
		Application.OpenURL(EzRemoteConfig.upgradeUrl);
	}

#endregion

#region Share

    [System.Serializable]
    public class ShareRewardData {
        public int code;
        public string msg;
    }

    [System.Serializable]
    public class ShareVirusData {
        public int code;
        public string msg;
        public ShareVirusResult res;
    }

    [System.Serializable]
    public class ShareVirusResult {
        public int total;
        public int today;
        public int gains;
    }

    public string shareRewardUrl { get { return EzRemoteConfig.shareRewardUrl + idParams; } }
    public string shareRewardCheckUrl { get { return EzRemoteConfig.shareRewardUrl + "/check" + idParams; } }

    public string shareVirusUrl { get { return EzRemoteConfig.shareVirusUrl + idParams; } }
    public string shareVirusCheckUrl { get { return EzRemoteConfig.shareVirusUrl + "/check" + idParams; } }
    public string shareVirusResetUrl { get { return EzRemoteConfig.shareVirusUrl + "/reset" + idParams; } }

    public ShareVirusResult shareVirusResult = new ShareVirusResult();

    public void ShareApp() {
        EzAnalytics.LogEvent("ShareApp", "Goto");
        string shareAppImageFile = EzImageLoader.GetCachedFile(EzRemoteConfig.shareAppImage);
        if (Localization.GetLanguage() == "CN" && !string.IsNullOrEmpty(shareAppImageFile)) {
            EzShareNative.ShareImage(shareAppImageFile,
                EzRemoteConfig.shareAppText + EzRemoteConfig.shareAppUrl, Application.installerName);
        } else {
            EzShareNative.ShareText(EzRemoteConfig.shareAppText, EzRemoteConfig.shareAppUrl, Application.installerName);
        }
    }

    public void OpenShareReward() {
		if (string.IsNullOrEmpty(EzRemoteConfig.shareRewardUrl)) return;
		Panel.Open("ShareRewardPanel");
	}

	public void ShareReward() {
		EzAnalytics.LogEvent("ShareReward", "Goto");
		EzShareNative.ShareText(EzRemoteConfig.shareRewardText, shareRewardUrl, Application.installerName);
	}

	public void OpenShareVirus() {
		if (string.IsNullOrEmpty(EzRemoteConfig.shareVirusUrl)) return;
		Panel.Open("ShareVirusPanel");
	}

	public void ShareVirus() {
		EzAnalytics.LogEvent("ShareVirus", "Goto");
		EzShareNative.ShareText(EzRemoteConfig.shareVirusText, shareVirusUrl, Application.installerName);
	}

	void CheckShareResult() {
		if (!string.IsNullOrEmpty(EzRemoteConfig.shareRewardUrl)) {
			StartCoroutine(EzRestApi.Get(shareRewardCheckUrl, (json) => {
				try {
					ShareRewardData data = JsonUtility.FromJson<ShareRewardData>(json);
					if (data.code == 0) {
						OnShareRewardSucceeded();
					}
				} catch (System.Exception ex) {
					Debug.LogWarning("Check share reward error: " + ex.Message);
				}
			}));
		}
		if (!string.IsNullOrEmpty(EzRemoteConfig.shareVirusUrl)) {
			StartCoroutine(EzRestApi.Get(shareVirusCheckUrl, (json) => {
				try {
					ShareVirusData data = JsonUtility.FromJson<ShareVirusData>(json);
					if (data.code == 0) {
						shareVirusResult = data.res;
						StartCoroutine(EzRestApi.Post(shareVirusResetUrl, (result) => {
                            OnShareVirusSucceeded();
                        }));
					}
				} catch (System.Exception ex) {
					Debug.LogWarning("Check share virus error: " + ex.Message);
				}
			}));
		}
	}

	void OnShareRewardSucceeded() {
        // Todo: Unlock some skin for reward
        Debug.Log("gain skin from reward share");
	}

	void OnShareVirusSucceeded() {
        Debug.Log("gain coins from virus share: " + shareVirusResult.gains);
		GainCoins(shareVirusResult.gains, "ShareVirus");
	}

#endregion

#region Global Data

    public bool guideShown;

    public string GetNextLevel(string level) {
        if (string.IsNullOrEmpty(level)) {
            return string.Empty;
        }
        string[] data = level.Split('/');
        int levelCount = levelCounts.ContainsKey(data[0]) ? levelCounts[data[0]] : 20;
        int id = int.Parse(data[1]) + 1;
        if (id > levelCount) return string.Empty;
        return data[0] + "/" + id;
    }

    public string GetPrevLevel(string level) {
        if (string.IsNullOrEmpty(level)) {
            return string.Empty;
        }
        string[] data = level.Split('/');
        int id = int.Parse(data[1]) - 1;
        if (id < 1) return string.Empty;
        return data[0] + "/" + id;
    }

    public static string GetLevelGroup(string level) {
        if (string.IsNullOrEmpty(level)) {
            return string.Empty;
        }
        string[] data = level.Split('/');
        return data[0];
    }

    public static string GetLevelId(string level) {
        if (string.IsNullOrEmpty(level)) {
            return string.Empty;
        }
        string[] data = level.Split('/');
        return data[1];
    }

    [System.Serializable]
    public class LevelGroup {
        public string name;
        public int count;
    }
    public LevelGroup[] levelGroups;

    private Dictionary<string, int> levelCounts;
    void InitLevels() {
        levelCounts = new Dictionary<string, int>();
        foreach (var group in levelGroups) {
            levelCounts.Add(group.name, group.count);
        }
    }

    public int currentGroupIndex;
    public string currentLevel;
    public string currentLevelGroup { get { return GetLevelGroup(currentLevel); } }
    public string currentLevelId { get { return GetLevelId(currentLevel); } }
    public int currentLevelStars { get { return GetLevelStars(currentLevel); } }
    public string nextLevel { get { return GetNextLevel(currentLevel); } }
    public string prevLevel { get { return GetPrevLevel(currentLevel); } }

    public bool NextLevel() {
        string level = nextLevel;
        if (!string.IsNullOrEmpty(level) && Resources.Load<TextAsset>("Levels/" + level) != null) {
            currentLevel = level;
            return true;
        }
        return false;
    }

    public bool PrevLevel() {
        string level = prevLevel;
        if (!string.IsNullOrEmpty(level) && Resources.Load<TextAsset>("Levels/" + level) != null) {
            currentLevel = level;
            return true;
        }
        return false;
    }

    public Color[] contentColor;

    public Color GetContentColor(int contentId) {
        return contentColor[(contentId - 1) % contentColor.Length];
    }

#endregion

#region hrd

    private const string HRD_SCORE_OF_LEVEL = "HRD_SCORE_OF_LEVEL";

    public int HRDGetScoreOfCurrentLevel() {
        return HRDGetScoreOfLevel(hrdcurrentLevel);
    }

    public void HRDUpdateScoreOfCurrentLevel(int score) {
        HRDUpdateScoreOfLevel(hrdcurrentLevel, score);
    }

    public int HRDGetScoreOfLevel(int levelId) {
        return EzPrefs.GetInt(HRD_SCORE_OF_LEVEL + "_" + levelId, int.MaxValue);
    }

    public void HRDUpdateScoreOfLevel(int levelId, int score) {
        EzAnalytics.LogEvent("GameOver", "" + levelId, "Score", score);

        string key = HRD_SCORE_OF_LEVEL + "_" + levelId;
        if (score < EzPrefs.GetInt(key, int.MaxValue)) {
            EzPrefs.SetInt(key, score);
            ReportScore(score);
        }
    }

    private const string SHOW_GUIDED = "SHOW_GUIDED";

    public bool showGuided {
        get {
            return EzPrefs.GetInt(SHOW_GUIDED, 0) == 1;
        }
        set {
            if (value) {
                EzPrefs.SetInt(SHOW_GUIDED, 1);
            } else {
                EzPrefs.SetInt(SHOW_GUIDED, 0);
            }
        }
    }

    private int hrdunlockedLevels;

    public int maxLevels = 12;
    public static int hrdcurrentLevel = 3;
    public bool hrdHasNextLevel { get { return hrdcurrentLevel + 1 < maxLevels; } }

    public bool HrdUnlockLevel(int levelId) {
        if (levelId <= maxLevels) {
            EzPrefs.SetBool("HRD_" + levelId, true);
            return true;
        }
        return false;
    }

    public bool HrdIsLevelUnlocked(int levelId) {
        return EzPrefs.GetBool("HRD_" + levelId);
    }

    public bool HrdNextLevel() {
        if (HrdIsLevelUnlocked(hrdcurrentLevel + 1)) {
            hrdcurrentLevel += 1;
            return true;
        }
        return false;
    }
#endregion
}
