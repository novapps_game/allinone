﻿using UnityEngine;
using System.Collections;

public class TweenMove : Tween {

    public Vector3 position;
    public bool offsetMode;
    public bool localSpace;
    public bool useLossyScale;

    public override void PlayNow() {
        if (offsetMode) {
            if (useLossyScale) position.Scale(transform.lossyScale);
            iTween.MoveBy(gameObject, iTween.Hash("amount", position,
                "time", time, "delay", delay, "space", localSpace ? Space.Self : Space.World,
                "easetype", easeType, "looptype", loopType,
                "onupdate", "OnUpdate", "oncomplete", "OnComplete"));
        } else {
            iTween.MoveTo(gameObject, iTween.Hash("position", position,
                "time", time, "delay", delay, "islocal", localSpace,
                "easetype", easeType, "looptype", loopType,
                "onupdate", "OnUpdate", "oncomplete", "OnComplete"));
        }
    }

    public override string Type() {
        return "move";
    }
}
