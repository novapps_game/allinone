﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TweenValue<T> : Tween {

    public T from;
    public T to;

    public override void PlayNow() {
        iTween.ValueTo(gameObject, iTween.Hash("from", from, "to", to,
            "time", time, "delay", delay,
            "easetype", easeType, "looptype", loopType,
            "onupdate", "OnValueUpdate", "oncomplete", "OnComplete"));
    }

    protected abstract void OnValueUpdate(T value);

    public override string Type() {
        return "value";
    }
}
