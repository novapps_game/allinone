﻿using UnityEngine;
using System.Collections;

public class TweenRotate : Tween {

    public bool rotation360;
    public Vector3 rotation;
    public bool localSpace;

    public override void PlayNow() {
        if (!rotation360) {
            iTween.RotateTo(gameObject, iTween.Hash("rotation", rotation,
                "time", time, "delay", delay, "islocal", localSpace,
                "easetype", easeType, "looptype", loopType,
                "onupdate", "OnUpdate", "oncomplete", "OnComplete"));
        } else {
            iTween.ValueTo(gameObject, iTween.Hash("from", transform.localEulerAngles, "to", transform.localEulerAngles + rotation,
            "time", time, "delay", delay,
            "easetype", easeType, "looptype", loopType,
            "onupdate", "OnValueUpdate", "oncomplete", "OnComplete"));
        }
    }

    public override string Type() {
        return "rotate";
    }

    public void OnValueUpdate(Vector3 value) {
        transform.localEulerAngles = value;
    }
}
