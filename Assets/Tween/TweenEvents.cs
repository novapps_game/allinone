﻿using UnityEngine;
using UnityEngine.Events;
using System;

public static class TweenEvents {

    [Serializable]
    public class OnUpdateEvent : UnityEvent<GameObject> { }

    [Serializable]
    public class OnCompleteEvent : UnityEvent<GameObject> { }
}
