﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LocalizedText : MonoBehaviour {

    public string key;
    public string prefix;
    public string suffix;

    // Use this for initialization
    void Start() {
        GetComponent<Text>().text = prefix + Localization.GetMultilineText(key) + suffix;
        ContentSizeFitter fitter = GetComponent<ContentSizeFitter>();
        if (fitter != null) {
            fitter.SetLayoutHorizontal();
        }
    }
}
