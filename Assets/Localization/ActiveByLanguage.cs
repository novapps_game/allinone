﻿using UnityEngine;
using System.Collections;

public class ActiveByLanguage : MonoBehaviour {

    public string language;

	// Use this for initialization
	void Start() {
        gameObject.SetActive(language.ToUpper() == Localization.GetLanguage());
	}
	
}
