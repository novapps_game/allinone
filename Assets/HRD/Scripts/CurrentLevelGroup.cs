﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrentLevelGroup : MonoBehaviour {

    private Text text;

    void Awake() {
        text = GetComponent<Text>();
    }

    void Start() {
        text.text = GameManager.instance.currentLevelGroup;
    }

    public void OnLevelChanged(string level) {
        text.text = GameManager.GetLevelGroup(level);
    }
}
