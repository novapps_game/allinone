﻿using System.Linq;
using System.Collections.Generic;

public class Flows {

    public IEnumerable<Flow> FlowList { get { return flowList; } }

    private readonly IList<Flow> flowList = new List<Flow>();

    public void AddFlow(Flow flow) {
        if (!ContainsPath(flow)) {
            flowList.Add(flow);
        }
    }

    public bool ContainsPath(Flow flow) {
        return flowList.Any(p => p.coordsList.SequenceEqual(flow.coordsList));
    }
}
