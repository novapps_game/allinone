﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorfulWord : MonoBehaviour {

    // Use this for initialization
    void Start() {
        char[] chars = Board.GetInstance().GetCharArray();
        string word = string.Empty;
        for (int i = 0; i < chars.Length; ++i) {
            Color color = Board.GetInstance().GetColor(chars[i]);
            word += "<color=" + color.ToHexString() + ">" + chars[i] + "</color>";
        }
        GetComponent<Text>().text = word;
    }

    // Update is called once per frame
    void Update() {

    }
}
