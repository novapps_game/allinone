﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HRDGameController : MonoBehaviour {

    private static HRDGameController _instance;
    public static HRDGameController GetInstance() {
        if (_instance == null) {
            _instance = FindObjectOfType<HRDGameController>();
        }
        return _instance;
    }

    public int minDiff = 3;

    public float borderSize = 10;
    public float padding = 2;
    public float margin = 5;

    public Image bg;
    public Image borderBg;

    public Transform gameBg;
    public Transform blockParent;

    public HRDBlock blockPrefab;

    public BgColor[] bgColors;

    private int _sameCount;
    public int sameCount {
        get {
            return _sameCount;
        }
        set {
            _sameCount = value;

            if(value == row * row - 1) {
                WillGameOver();
            }
        }
    }

    private bool canClick {
        get {
            return !pause && (lastClickBlock == null || lastClickBlock.moving == false);
        }
    }

    public float time { get; set; }
    public float step { get; set; }

    public int row { get; set; }

    private HRDBlock lastClickBlock;

    private Vector2 currentPoint;
    private Vector2 currentPosition;

    private bool pause;

    private void Awake() {
        if(!GameManager.instance.showGuided) {
            GameManager.instance.showGuided = true;
            OpenHelp();
        }

        row = GameManager.hrdcurrentLevel;
    }

    private void Start() {
        EzAnalytics.LogEvent("HRD", "Level" + row, "Start");

        BgColor bgColor = bgColors[Random.Range(0, bgColors.Length)];
        bg.color = bgColor.bgColor;
        borderBg.color = bgColor.borderColor;

        Vector2 size = new Vector2(Screen.width / transform.localScale.x, Screen.height / transform.localScale.y);

        float itemSize = (size.x - borderSize * 2 - margin * 2 - (row - 1) * padding) / row;

        float bgSize = itemSize * row + (row - 1) * padding + margin * 2;
        gameBg.localScale = new Vector2(bgSize / 100, bgSize / 100);

        int[,] nums = new int[row,row];
        for (int i = 0; i < row; ++i) {
            for (int j = 0; j < row; ++j) {
                if (i * row + j + 1 == row * row) {
                    nums[i, j] = -1;
                } else {
                    nums[i, j] = i * row + j + 1;
                }
            }
        }

        Shuffle(nums, row * row * row * row);

        for (int i = 0; i < row * row; ++i) {
            int r = i / row;
            int c = i % row;
            Vector2 pointTemp = new Vector2(r, c);

            float x = -bgSize / 2 + (c * 2 + 1) * itemSize / 2 + margin + c * padding;
            float y = bgSize / 2 - (r * 2 + 1) * itemSize / 2 - margin - r * padding;
            Vector2 positionTemp = new Vector2(x, y);

            if (i == row * row - 1) {
                currentPoint = pointTemp;
                currentPosition = positionTemp;
            } else {
                HRDBlock block = Instantiate(blockPrefab);
                block.transform.SetParent(blockParent, false);
                block.scale = Vector3.one * (itemSize / 100);
                block.index = nums[r,c];
                block.point = pointTemp;
                block.position = positionTemp;
                block.color = bgColor.blockColor;
            }
        }
    }

    void Shuffle(int[,] nums, int n) {
        int maxX = nums.GetLength(0);
        int maxY = nums.GetLength(1);
        int x = maxX - 1;
        int y = maxY - 1;
        int lastOldX = -1;
        int lastOldY = -1;
        bool leftTop = false;
        for (int i = 0; i < n; i++) {
            int indexX = x;
            int indexY = y;

            if(leftTop) {
                GetRoundIndex(x, y, nums.GetLength(0), maxY,out indexX, out indexY, lastOldX, lastOldY);
            } else {
                if(x > y) {
                    indexX = x - 1;
                } else {
                    indexY = y - 1;
                }
                if(indexX == 0 && indexY == 0) {
                    leftTop = true;
                }
            }

            lastOldX = x;
            lastOldY = y;

            int tempValue = nums[indexX, indexY];
            nums[indexX, indexY] = nums[x, y];
            nums[x, y] = tempValue;

            x = indexX;
            y = indexY;
        }
        
        for(int i = x + 1;i < maxX; ++i) {
            nums[i - 1, y] = nums[i, y];
        }
        for (int i = y + 1; i < maxY; ++i) {
            nums[maxX - 1, i - 1] = nums[maxX - 1, i];
        }
        nums[maxX - 1, maxY - 1] = -1;
    }

    void GetRoundIndex(int x,int y,int maxX,int maxY, out int indexX, out int indexY, int lastOldX,int lastOldY) {
        List<Vector2> points = new List<Vector2>();
        if (x - 1 >= 0 && x -1 != lastOldX)
            points.Add(new Vector2(x-1, y));
        if(x + 1 < maxX && x + 1 != lastOldX)
            points.Add(new Vector2(x + 1, y));
        if (y - 1 >= 0 && y - 1 != lastOldY)
            points.Add(new Vector2(x, y -1));
        if (y + 1 < maxY && y + 1 != lastOldY)
            points.Add(new Vector2(x, y + 1));

        Vector2 index = points[Random.Range(0, points.Count)];
        indexX = (int)index.x;
        indexY = (int)index.y;
    }

    int SameNumWithIndex(int[,] nums) {
        int count = 0;
        for(int i = 0;i < nums.GetLength(0); ++i) {
            for (int j = 0; j < nums.GetLength(1); ++j) {
                if (nums[i,j] == i * row + j + 1) {
                    count++;
                }
            }
        }
        return count;
    } 

    private void Update() {
        if(!pause) {
            time += Time.deltaTime;
        }
    }

    public void OnItemClick(HRDBlock block) {
        if (!canClick || Vector2.Distance(currentPoint, block.point) != 1) return;

        step++;
        lastClickBlock = block;

        Vector2 tempPoint = block.point;
        Vector2 tempPosition = block.position;
        block.Move(currentPosition);
        block.point = currentPoint;
        currentPosition = tempPosition;
        currentPoint = tempPoint;

        GameManager.instance.PlaySound("move");
        if (GameManager.instance.testWin) {
            WillGameOver();
        }
    }

    public void OpenHelp() {
        pause = true;
        Panel.Open("HelpPanel");
    }

    public void CloseHelp() {
        pause = false;
    }

    public void WillGameOver() {
        pause = true;

#if UNITY_IOS
        if (row == 4 && Mathf.FloorToInt(HRDGameController.GetInstance().time) < 40) {
            GameManager.instance.purchased = true;
        }
#endif

        GameManager.instance.HRDUpdateScoreOfCurrentLevel(Mathf.FloorToInt(time));

        foreach (HRDBlock block in blockParent.GetComponentsInChildren<HRDBlock>()) {
            block.PlayScale();
        }

        Invoke("DoGameOver", 3);
    }

    public void DoGameOver() {
        GameManager.instance.GameOver();
    }
}

[System.Serializable]
public class BgColor {
    public Color bgColor;
    public Color borderColor;
    public Color blockColor;
}