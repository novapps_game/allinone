﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HRDLevelButton : MonoBehaviour {

    private GameObject coverImage;
    private GameObject lockImage;

    private int index {
        get {
            return int.Parse(name);
        }
    }

    // Use this for initialization
    void Start() {
        //coverImage = transform.Find("Cover").gameObject;
        //coverImage.SetActive(!GameManager.instance.HrdIsLevelUnlocked(index));
        lockImage = transform.Find("Lock").gameObject;
        GetComponent<Button>().onClick.AddListener(() => {
            if (GameManager.instance.HrdIsLevelUnlocked(index)) {
                SelectedLevel();
            } else if (EzAds.IsRewardedVideoLoaded()) {
                Panel.Open("UnlockLevelPanel", null, (action) => {
                    if (action == "OK") {
                        EzAds.ShowRewardedVideo((_) => {
                            GameManager.instance.HrdUnlockLevel(index);
                            Panel.Close("UnlockLevelPanel");
                        });
                    }
                });
            }
        });
    }

    void Update() {
        lockImage.SetActive(!GameManager.instance.HrdIsLevelUnlocked(index));
    }

    void SelectedLevel() {
        GameManager.hrdcurrentLevel = index;
        GameManager.instance.LoadScene("HRDGame");
    }
}
