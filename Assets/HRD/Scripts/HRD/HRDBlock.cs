﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HRDBlock : MonoBehaviour {

    public Image image;
    public Text text;
    public float moveTime = 0.2f;
    public float scaleValue = 1.2f;
    public float scaleTime = 0.5f;
    public float scaleDelay = 0.3f;

    public Color color {
        get {
            return image.color;
        }
        set {
            image.color = value;
        }
    }

    private int _index;
    public int index {
        get {
            return _index;
        }
        set {
            _index = value;

            name = value.ToString();
            text.text = name;
        }
    }

    private Vector2 _point = -Vector2.one;
    public Vector2 point {
        get {
            return _point;
        }
        set {
            if (_point.x * HRDGameController.GetInstance().row + _point.y == index - 1) {
                HRDGameController.GetInstance().sameCount--;
            }

            _point = value;
            if (_point.x * HRDGameController.GetInstance().row + _point.y == index - 1) {
                HRDGameController.GetInstance().sameCount++;
            }
        }
    }

    public Vector2 position {
        get {
            return transform.localPosition;
        }
        set {
            transform.localPosition = value;
        }
    }

    public Vector3 scale {
        get {
            return transform.localScale;
        }
        set {
            transform.localScale = value;
        }
    }

    public bool moving { get; set; }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void OnClick() {
        HRDGameController.GetInstance().OnItemClick(this);
    }

    public void Move(Vector2 p) {
        moving = true;

        iTween.MoveTo(gameObject, iTween.Hash("position", p.ToV3(), "time", moveTime, "islocal", true,
                "easetype", iTween.EaseType.linear, "oncomplete", "OnMoveComplete"));
    }

    public void OnMoveComplete() {
        moving = false;
    }

    public void PlayScale() {
        if (index == 15)
            Debug.Log("");
        iTween.ValueTo(gameObject, iTween.Hash("from", 1f, "to", 1 +(scaleValue - 1) * 2,
            "time", scaleTime, "delay", (index - 1) * scaleDelay,
            "easetype", iTween.EaseType.linear,
            "onupdate", "OnScaleUpdate", "oncomplete", "OnScaleComplete"));
    }

    public void OnScaleUpdate(float scale) {
        if(scale > scaleValue) {
            scale = scaleValue * 2 - scale;
        }
        text.transform.localScale = Vector3.one * scale;
    } 

    public void OnScaleComplete() {
        text.color = Color.green;
    }
}