﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HRDHomeController : MonoBehaviour {

    public GameObject mergeObj;
    public GameObject hrdObj;

    // Use this for initialization
    void Start () {
#if UNITY_ANDROID
        hrdObj.SetActive(true);
        mergeObj.SetActive(false);
#elif UNITY_IOS
        hrdObj.SetActive(false);
        mergeObj.SetActive(true);
#endif
    }

    // Update is called once per frame
    void Update () {
		
	}
}
