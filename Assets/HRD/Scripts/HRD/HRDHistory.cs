﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HRDHistory : MonoBehaviour {

    public Text text;

	// Use this for initialization
	void Start () {
        if (GameManager.instance.HRDGetScoreOfCurrentLevel() == int.MaxValue)
            gameObject.SetActive(false);

        text.text = Localization.GetText("HistoryRecord") + GameManager.instance.HRDGetScoreOfCurrentLevel() + "s";
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
