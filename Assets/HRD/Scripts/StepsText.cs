﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StepsText : MonoBehaviour {

    public Color alertColor;

    private Text text;
    private Color originColor;

	void Awake() {
        text = GetComponent<Text>();
        originColor = text.color;
    }
	
	public void OnStepsChanged(int steps) {
        text.text = steps.ToString();
        if (steps > Board.GetInstance().flows) {
            text.color = alertColor;
        } else {
            text.color = originColor;
        }
    }
}
