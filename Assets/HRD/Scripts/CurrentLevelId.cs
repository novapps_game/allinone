﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrentLevelId : MonoBehaviour {

    private Text text;

    void Awake() {
        text = GetComponent<Text>();
    }

    void Start() {
        text.text = string.Format(Localization.GetText("LevelFormat"), GameManager.instance.currentLevelId);
    }

    public void OnLevelChanged(string level) {
        text.text = string.Format(Localization.GetText("LevelFormat"), GameManager.GetLevelId(level));
    }
}
