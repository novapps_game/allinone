﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Conditions : MonoBehaviour {

    public float fadeInterval = 0.5f;

    public Sprite failSprite;

    // Use this for initialization
    void Start() {
        for (int i = 0; i < transform.childCount; ++i) {
            float alpha = 1;
            if ((i == 1 && !Board.GetInstance().correctWord) ||
                (i == 2 && !Board.GetInstance().useMinSteps)) {
                transform.GetChild(i).Find("Check").GetComponent<Image>().sprite = failSprite;
                alpha = 0.5f;
            }
            iTween.FadeTo(transform.GetChild(i).gameObject, iTween.Hash("alpha", alpha, "time", 0.5f,
                "delay", i * fadeInterval, "easetype", iTween.EaseType.linear));
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
