﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour {

    public int id = 1;
    private LevelGroupItem group;

	// Use this for initialization
	void Start() {
        transform.Find("Text").GetComponent<Text>().text = id.ToString();
        group = GetComponentInParent<LevelGroupItem>();
        name = group.name + "/" + id;
        GetComponent<Button>().onClick.AddListener(() => {
            GameManager.instance.currentGroupIndex = group.index;
            GameManager.instance.currentLevel = name;
            GameManager.instance.SwitchScene("Game");
        });
        Transform stars = transform.Find("Stars");
        int starCount = GameManager.instance.GetLevelStars(name);
        if (starCount == 0) {
            stars.gameObject.SetActive(false);
        } else {
            for (int i = 0; i < starCount; ++i) {
                Image image = stars.GetChild(i).GetComponent<Image>();
                image.color = image.color.NewA(1);
            }
        }
	}
}
