﻿[System.Serializable]
public class Pair {

    public Coords startCoords;
    public Coords endCoords;
    public int contentId;

    public Pair(int contentId) {
        this.contentId = contentId;
    }

    public Pair(Coords startCoords, Coords endCoords, int contentId) {
        this.startCoords = startCoords;
        this.endCoords = endCoords;
        this.contentId = contentId;
    }
}
