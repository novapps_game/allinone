﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGroupList : ScrollList {

	void Awake() {
        itemCount = GameManager.instance.levelGroups.Length;
        initIndex = GameManager.instance.currentGroupIndex;
    }
}
