﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Word : MonoBehaviour {

    private Text[] texts;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void OnFlowsChanged(string flows) {
        int length = int.Parse(flows);
        GameObject prefab = transform.GetChild(0).gameObject;
        texts = new Text[length];
        int i = 0;
        for (; i < length; ++i) {
            if (i < transform.childCount) {
                Transform child = transform.GetChild(i);
                child.gameObject.SetActive(true);
                texts[i] = child.GetComponentInChildren<Text>(true);
            } else {
                Transform child = Instantiate(prefab).transform;
                child.SetParent(transform, false);
                child.name = prefab.name;
                texts[i] = child.GetComponentInChildren<Text>(true);
            }
        }
        for (; i < transform.childCount; ++i) {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    public void OnWordChanged(string word) {
        char[] chars = word.ToCharArray();
        for (int i = 0; i < texts.Length; ++i) {
            Text text = texts[i];
            if (i < chars.Length) {
                text.text = chars[i].ToString();
                text.color = Board.GetInstance().GetColor(chars[i]);
                text.gameObject.SetActive(true);
            } else {
                text.gameObject.SetActive(false);
            }
        }
    }
}
