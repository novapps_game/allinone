﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cell : MonoBehaviour {

    public int col { get; set; }
    public int row { get; set; }

    public int contentId {
        get { return _contentId; }
        set {
            if (_contentId != value) {
                _contentId = value;
                if (contentId > 0) {
                    dot.gameObject.SetActive(true);
                    dot.color = GameManager.instance.GetContentColor(contentId);
                    id.text = Board.GetInstance().GetChar(contentId);
                } else {
                    dot.gameObject.SetActive(false);
                }
            }
        }
    }
    private int _contentId;
    public int lastId;
    public int currentId;

    public Vector2 linePoint { get { return rectTransform.position; } }

    public bool highlighted { get { return cover.gameObject.activeSelf; } }

    private RectTransform rectTransform;
    private Image bg;
    private Image cover;
    private Image dot;
    private Text id;
    private Vector3 dotScale;

	void Awake() {
        rectTransform = GetComponent<RectTransform>();
        bg = transform.Find("Bg").GetComponent<Image>();
        cover = bg.transform.Find("Cover").GetComponent<Image>();
        dot = transform.Find("Dot").GetComponent<Image>();
        id = dot.transform.Find("Id").GetComponent<Text>();
        dotScale = dot.transform.localScale;
    }
	
	// Update is called once per frame
	void Update() {
		if (contentId > 0) {
            id.text = Board.GetInstance().GetChar(contentId);
        }
	}

    public void Reset() {
        contentId = 0;
        lastId = 0;
        currentId = 0;
        dot.gameObject.SetActive(false);
        cover.gameObject.SetActive(false);
    }

    public bool IsNeighbor(Cell other) {
        if (col == other.col && Mathf.Abs(row - other.row) == 1) return true;
        if (row == other.row && Mathf.Abs(col - other.col) == 1) return true;
        return false;
    }

    public void Win(float delay, float duration) {
        iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.one * 0.9f, "time", duration, 
            "delay", delay, "easetype", iTween.EaseType.easeInSine, 
            "oncomplete", "OnWon", "oncompleteparams", duration));
    }

    void OnWon(float duration) {
        iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.one, "time", duration,
            "easetype", iTween.EaseType.easeOutSine));
    }

    public void OnSelect() {
        iTween.ScaleTo(dot.gameObject, iTween.Hash("scale", dotScale * 1.1f, "time", 0.2f,
            "easetype", iTween.EaseType.easeInSine, "oncomplete", "OnScaled", "oncompletetarget", gameObject));
    }

    void OnScaled() {
        iTween.ScaleTo(dot.gameObject, iTween.Hash("scale", dotScale, "time", 0.4f,
            "easetype", iTween.EaseType.easeOutBounce));
    }

    public void Highlight() {
        Highlight(contentId);
    }

    public void Highlight(int id) {
        if (id > 0 && currentId != id) {
            lastId = currentId;
            if (lastId > 0) {
                Board.GetInstance().SetRouteDirty(lastId);
            }
            currentId = id;
            cover.color = cover.color.NewRGB(GameManager.instance.GetContentColor(currentId));
            cover.gameObject.SetActive(true);
            Board.GetInstance().SetRouteDirty(currentId);
        }
    }

    public void Dehighlight() {
        if (lastId > 0) {
            if (currentId > 0) {
                Board.GetInstance().SetRouteDirty(currentId);
            }
            currentId = lastId;
            lastId = 0;
            cover.color = cover.color.NewRGB(GameManager.instance.GetContentColor(currentId));
            cover.gameObject.SetActive(true);
            Board.GetInstance().SetRouteDirty(currentId);
        } else {
            Unhighlight();
        }
    }

    public void Unhighlight() {
        if (currentId > 0) {
            Board.GetInstance().SetRouteDirty(currentId);
        }
        lastId = currentId = 0;
        cover.gameObject.SetActive(false);
    }
}
