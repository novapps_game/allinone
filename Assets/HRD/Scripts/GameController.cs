﻿using DlxLib;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameController : Singleton<GameController> {

    // Use this for initialization
    void Start() {
        GameManager.instance.OnGameStart();
        if (GameManager.instance.debug || GameManager.instance.playTimes < 2) {
            if (!GameManager.instance.guideShown) {
                GameManager.instance.guideShown = true;
                Panel.Open("GuidePanel");
            }
        }
    }

    // Update is called once per frame
    void Update() {
        Panel.CheckBackButton(() => GameManager.instance.LoadScene("Menu"));
    }

    public void Revive() {
        // Todo: revive player
    }

    public void Hint() {
        if (GameManager.instance.debug) {
            Board.GetInstance().Hint();
        } else {
            EzAds.ShowRewardedVideo((_) => {
                Board.GetInstance().Hint();
            });
        }
    }

    public void LevelComplete(int stars) {
        Debug.Log("Level complete!");
        GameManager.instance.SetCurrentLevelStars(stars);
        if (!GameManager.instance.CheckRate()) {
            GameManager.instance.CheckAds();
        }
        Panel.Open("LevelCompletePanel");
    }

    public void GameOver() {
        GameManager.instance.OnGameOver();
    }

}
