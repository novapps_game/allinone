﻿[System.Serializable]
public class Coords {

    public int x;
    public int y;

    public Coords(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public override bool Equals(object rhs) {
        if (rhs == null)
            return false;

        if (ReferenceEquals(this, rhs))
            return true;

        if (GetType() != rhs.GetType())
            return false;

        return CompareFields(rhs as Coords);
    }

    public override int GetHashCode() {
        unchecked {
            var hash = 17;
            hash = hash * 23 + x.GetHashCode();
            hash = hash * 23 + y.GetHashCode();
            return hash;
        }
    }

    public override string ToString() {
        return string.Format("({0}, {1})", x, y);
    }

    private bool CompareFields(Coords rhs) {
        return (x == rhs.x && y == rhs.y);
    }

    public bool IsNeighbor(Coords other) {
        int dx = other.x - x;
        int dy = other.y - y;
        return (dx == 0 && (dy == 1 || dy == -1)) ||
            (dy == 0 && (dx == 1 || dx == -1));
    }

    public Flow StartPath() {
        var path = new Flow();
        path.AddCoords(this);
        return path;
    }

}
