﻿[System.Serializable]
public class Link {

    public Pair pair;
    public Flow flow;

    public Link(Pair pair) {
        this.pair = pair;
        flow = new Flow();
    }

    public Link(Pair pair, Flow flow) {
        this.pair = pair;
        this.flow = flow;
    }
}
