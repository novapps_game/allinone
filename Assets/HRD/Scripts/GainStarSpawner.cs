﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GainStarSpawner : EzPool<ParticleSystem, GainStarSpawner> {

	public static ParticleSystem Spawn(Vector3 position) {
        ParticleSystem ps = instance.Create();
        ps.transform.position = position;
        ps.Play();
        return ps;
    }
}
