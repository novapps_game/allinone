﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Route : LinkedList<Cell> {

    public bool IsValid {
        get {
            if (Count < 2) return false;
            if (First.Value.contentId == 0 || Last.Value.contentId == 0 ||
                First.Value.contentId != Last.Value.contentId) return false;
            for (var node = First; node != Last; node = node.Next) {
                if (node.Value.currentId != node.Next.Value.currentId) {
                    return false;
                }
                if (Mathf.Abs(node.Value.col - node.Next.Value.col) +
                    Mathf.Abs(node.Value.row - node.Next.Value.row) != 1) {
                    return false;
                }
            }
            return true;
        }
    }

    public Vector3[] ValidPoints {
        get {
            var node = First;
            List<Vector3> pts1 = new List<Vector3>();
            for (; node != null && node.Value.currentId == First.Value.contentId; node = node.Next) {
                if (node.Value.highlighted) {
                    pts1.Add(node.Value.linePoint);
                }
            }
            if (node != null && Last.Value.contentId > 0) {
                List<Vector3> pts2 = new List<Vector3>();
                for (node = Last; node != null && node.Value.currentId == Last.Value.contentId; node = node.Previous) {
                    if (node.Value.highlighted) {
                        pts2.Add(node.Value.linePoint);
                    }
                }
                if (pts1.Count < pts2.Count) {
                    pts1 = pts2;
                }
            }
            return pts1.ToArray();
        }
    }

    public bool Dirty { get; set; }

    public Route Copy() {
        var copy = new Route();
        for (var node = First; node != null; node = node.Next) {
            copy.AddLast(node.Value);
        }
        return copy;
    }

    public void Reverse() {
        var head = First;
        while (head.Next != null) {
            var node = head.Next;
            Remove(node);
            AddFirst(node.Value);
        }
    }

    public bool IsSameTo(Route other) {
        if (Count != other.Count) return false;
        var n1 = First;
        var n2 = other.First;
        while (n1 != null && n2 != null) {
            if (n1.Value != n2.Value) {
                n1 = First;
                n2 = other.Last;
                while (n1 != null && n2 != null) {
                    if (n1.Value != n2.Value) {
                        return false;
                    }
                    n1 = n1.Next;
                    n2 = n2.Previous;
                }
                return true;
            }
            n1 = n1.Next;
            n2 = n2.Next;
        }
        return true;
    }

    public void BreakAt(LinkedListNode<Cell> node) {
        while (Last != node) {
            Last.Value.Dehighlight();
            RemoveLast();
        }
    }

    public void BreakFrom(Cell cell) {
        int distanceWithStart = 0;
        int distanceWithEnd = 0;
        LinkedListNode<Cell> node = null;
        for (var n = First.Next; n != null; n = n.Next) {
            if (n.Value.contentId > 0) continue;
            if (distanceWithEnd > 0) {
                ++distanceWithEnd;
            } else {
                ++distanceWithStart;
                if (n.Value == cell) {
                    distanceWithEnd = 1;
                    node = n;
                }
            }
        }
        if (node != null) {
            int id = First.Value.contentId;
            if (First.Value.contentId > 0 && Last.Value.contentId > 0) {
                if (distanceWithStart < distanceWithEnd) {
                    while (First != node) {
                        if (First.Value.currentId == id) {
                            First.Value.Unhighlight();
                        }
                        RemoveFirst();
                    }
                    RemoveFirst();
                    Reverse();
                } else {
                    while (Last != node) {
                        if (Last.Value.currentId == id) {
                            Last.Value.Unhighlight();
                        }
                        RemoveLast();
                    }
                    RemoveLast();
                }
            } else if (First.Value.contentId > 0) {
                while (Last != node) {
                    if (Last.Value.currentId == id) {
                        Last.Value.Unhighlight();
                    }
                    RemoveLast();
                }
                RemoveLast();
            } else if (Last.Value.contentId > 0) {
                while (First != node) {
                    if (First.Value.currentId == id) {
                        First.Value.Unhighlight();
                    }
                    RemoveFirst();
                }
                RemoveFirst();
                Reverse();
            }
        }
    }

    public void Highlight() {
        for (var node = First; node != null; node = node.Next) {
            node.Value.Highlight(First.Value.contentId);
        }
    }

    public void Unhighlight() {
        for (var node = First; node != null; node = node.Next) {
            node.Value.Unhighlight();
        }
    }

    public void Reset() {
        if (Count > 0) {
            for (var node = First; node != null; node = node.Next) {
                node.Value.Unhighlight();
            }
            Clear();
        }
    }

    public void Win(float interval, float duration) {
        int i = 0;
        for (var node = First; node != null; node = node.Next) {
            node.Value.Win(interval * i++, duration);
        }
    }
}
