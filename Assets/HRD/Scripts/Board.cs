﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Board : EzPool<Cell, Board>, IPointerDownHandler, IDragHandler, IPointerUpHandler, IPointerClickHandler {

    public GameObject linePrefab;
    public Transform cursor;

    public bool editable;

    public int maxFlows { get; set; }

    public bool squareSize { get; set; }

    public float cellSize { get { return (rectTransform.sizeDelta.x - 8) / cols; } }

    public int cols {
        get { return gridLayoutGroup.constraintCount; }
        set {
            gridLayoutGroup.constraintCount = value;
            float size = cellSize;
            gridLayoutGroup.cellSize = new Vector2(size, size);
        }
    }

    public int rows {
        get {
            if (_rows == 0) _rows = cols;
            return _rows;
        }
        set {
            if (value > 0 && value != _rows) {
                _rows = value;
            }
        }
    }
    private int _rows;

    public int flows { get { return grid.links.Length; }  }

    private int cellCount { get { return rows * cols; } }
    private int highlights {
        get {
            int count = 0;
            foreach (var route in routes) {
                if (route != null && route.IsValid) {
                    count += route.Count;
                }
            }
            return count;
        }
    }
    private bool over;
    private bool won { get { return highlights == cellCount; } }
    public bool correctWord { get { return currentWord == grid.word; } }
    public bool useMinSteps { get { return steps == grid.links.Length; } }

    public int stars {
        get {
            int n = 1;
            if (correctWord) ++n;
            if (useMinSteps) ++n;
            return n;
        }
    }

    [System.Serializable]
    public class BoolChangeEvent : UnityEvent<bool> { }

    [System.Serializable]
    public class FloatChangeEvent : UnityEvent<float> { }

    [System.Serializable]
    public class IntChangeEvent : UnityEvent<int> { }

    [System.Serializable]
    public class StringChangeEvent : UnityEvent<string> { }

    public FloatChangeEvent onColsChange;
    public FloatChangeEvent onRowsChange;
    public IntChangeEvent onSelectChange;
    public StringChangeEvent onWordChange;
    public StringChangeEvent onCurrentWordChange;
    public StringChangeEvent onMaxFlowsChange;
    public StringChangeEvent onFlowsChange;
    public StringChangeEvent onLevelChange;
    public IntChangeEvent onStepsChange;
    public BoolChangeEvent onInteractableChange;

    public string level { get; set; }
    public string levelAssetPath { get { return "Levels/" + level; } }
    public string levelFilePath { get { return Application.dataPath + "/Resources/" + levelAssetPath + ".txt"; } }

    public int selectedContentId { get; set; }

    private RectTransform rectTransform;
    private RectTransform canvasTransform;
    private GridLayoutGroup gridLayoutGroup;
    private Grid grid;
    private char[] characters;
    private int[] charIndices;
    private Dictionary<char, int> charToIds;
    private string currentWord;
    private int steps;
    private Cell[,] cells;
    private Route[] routes;
    private List<Route> solution;
    private Route curRoute;
    private Transform lines;
    private LineRenderer[] lineRenderers;
    private SpriteRenderer cursorRenderer;

    void Awake() {
        squareSize = true;
        rectTransform = GetComponent<RectTransform>();
        gridLayoutGroup = GetComponent<GridLayoutGroup>();
        lines = new GameObject("Lines").transform;
        if (cursor != null) {
            cursorRenderer = cursor.GetComponent<SpriteRenderer>();
        }
    }

    // Use this for initialization
    void Start() {
        level = GameManager.instance.currentLevel;
        if (onLevelChange != null) {
            onLevelChange.Invoke(level);
        }
        selectedContentId = 1;
        Init();
        Load();
    }

    // Update is called once per frame
    void Update() {
        if (linePrefab != null) {
            for (int i = 0; i < routes.Length; ++i) {
                if (routes[i] != null && routes[i].Dirty) {
                    Vector3[] points = routes[i].ValidPoints;
                    lineRenderers[i].positionCount = points.Length;
                    lineRenderers[i].SetPositions(points);
                    routes[i].Dirty = false;
                }
            }
        }
    }

    public override void Recycle(Cell cell) {
        cell.Reset();
        base.Recycle(cell);
    }

    void Init() {
        cells = new Cell[rows, cols];
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                cells[i, j] = Create();
                cells[i, j].row = i;
                cells[i, j].col = j;
                cells[i, j].name = "Cell" + "(" + i + "," + j + ")";
            }
        }
    }

    void Recycle() {
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                Recycle(cells[i, j]);
            }
        }
    }

    void Clear() {
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                cells[i, j].Reset();
            }
        }
    }

    public void OnWidthChanged(float width) {
        int w = Mathf.RoundToInt(width);
        if (cols != w) {
            Recycle();
            cols = w;
            if (squareSize) {
                rows = w;
                if (onRowsChange != null) {
                    onRowsChange.Invoke(rows);
                }
                maxFlows = rows;
                if (onMaxFlowsChange != null) {
                    onMaxFlowsChange.Invoke(maxFlows.ToString());
                }
            }
            level = cols + "x" + rows + "/1";
            if (onLevelChange != null) {
                onLevelChange.Invoke(level);
            }
            Init();
        }
    }

    public void OnHeightChanged(float height) {
        int h = Mathf.RoundToInt(height);
        if (rows != h) {
            Recycle();
            rows = h;
            level = cols + "x" + rows + "/1";
            if (onLevelChange != null) {
                onLevelChange.Invoke(level);
            }
            Init();
        }
    }

    public void OnSelectContentId(int index) {
        if (selectedContentId != index + 1) {
            selectedContentId = index + 1;
        }
    }

    bool CheckDuplicateChars(out char dup) {
        dup = ' ';
        for (int i = 0; i < characters.Length - 1; ++i) {
            dup = characters[i];
            for (int j = i + 1; j < characters.Length; ++j) {
                if (dup == characters[j]) {
                    return true;
                }
            }
        }
        return false;
    }

    public void OnWordChanged(string word) {
        word = word.ToUpper();
        characters = word.ToCharArray();
        char dup;
        if (CheckDuplicateChars(out dup)) {
            Debug.LogWarning("Word has duplicate char: '" + dup + "'");
        } else if (grid.word.ToUpper() != word) {
            grid.word = word;
        }
    }

    public void OnMaxFlowsChanged(string text) {
        maxFlows = int.Parse(text);
    }

    public void ManualSetContentId(int col, int row, int id) {
        grid.ManualSetContentId(col, row, id);
        Dehighlight();
    }

    public void ChangeSelectContentId(int id) {
        if (selectedContentId != id) {
            selectedContentId = id;
            if (onSelectChange != null) {
                onSelectChange.Invoke(id - 1);
            }
        }
    }

    public void OnLevelChanged(string level) {
        this.level = level;
    }

    public void Generate() {
        Clear();
        grid = new Grid(cols, rows);
        int flows = 0;
        do {
            flows = grid.Generate();
        } while (flows > maxFlows || grid.HasTwin());
        if (onWordChange != null) {
            onWordChange.Invoke(grid.word);
        }
        characters = grid.word.ToCharArray();
        if (onFlowsChange != null) {
            onFlowsChange.Invoke(flows.ToString());
        }
        charIndices = new int[grid.indices.Length];
        for (int i = 0; i < grid.indices.Length; ++i) {
            charIndices[grid.indices[i]] = i;
        }
        foreach (var link in grid.links) {
            cells[link.pair.startCoords.y, link.pair.startCoords.x].contentId = link.pair.contentId;
            cells[link.pair.endCoords.y, link.pair.endCoords.x].contentId = link.pair.contentId;
        }
        routes = new Route[grid.links.Length];
    }

    public void Solve() {
        grid.PrintCells();
        if (grid.Solve()) {
            grid.PrintSolution();
            Highlight();
        }
    }

    public void Highlight() {
        foreach (var link in grid.links) {
            cells[link.pair.startCoords.y, link.pair.startCoords.x].Highlight();
            cells[link.pair.endCoords.y, link.pair.endCoords.x].Highlight();
            int contentId = link.pair.contentId;
            foreach (var coords in link.flow.coordsList) {
                cells[coords.y, coords.x].Highlight(contentId);
            }
        }
    }

    public void Dehighlight() {
        foreach (var cell in cells) {
            cell.Unhighlight();
        }
    }

    public void Load(string level) {
        if (this.level != level) {
            this.level = level;
            if (onLevelChange != null) {
                onLevelChange.Invoke(level);
            }
            Load();
        }
    }

    public void Load() {
        TextAsset levelData = Resources.Load<TextAsset>(levelAssetPath);
        if (levelData == null) {
            Debug.LogWarning("Level data file: " + levelFilePath + " not exists.");
            return;
        }
        over = false;
        if (onInteractableChange != null) {
            onInteractableChange.Invoke(over);
        }
        grid = SaveHelper.LoadData<Grid>(levelData.text, false);
        if (onFlowsChange != null) {
            onFlowsChange.Invoke(grid.links.Length.ToString());
        }
        if (grid.word == null) {
            grid.word = string.Empty;
        } else {
            grid.word = grid.word.ToUpper();
        }
        if (onWordChange != null) {
            onWordChange.Invoke(grid.word);
        }
        characters = grid.word.ToCharArray();
        if (grid.indices == null) {
            grid.indices = EzUtil.RandomPerm(0, grid.links.Length);
        }
        charIndices = new int[grid.indices.Length];
        charToIds = new Dictionary<char, int>();
        for (int i = 0; i < grid.indices.Length; ++i) {
            charIndices[grid.indices[i]] = i;
            if (characters.Length > 0 && !charToIds.ContainsKey(characters[i])) {
                charToIds.Add(characters[i], grid.indices[i] + 1);
            }
        }
        grid.Fill();
        if (grid.width != cols || grid.height != rows) {
            Recycle();
            cols = grid.width;
            if (onColsChange != null) onColsChange.Invoke(cols);
            rows = grid.height;
            if (onRowsChange != null) onRowsChange.Invoke(rows);
            maxFlows = Mathf.RoundToInt(Mathf.Sqrt(cols * rows));
            if (onMaxFlowsChange != null) onMaxFlowsChange.Invoke(maxFlows.ToString());
            Init();
        } else {
            Clear();
        }
        solution = new List<Route>();
        foreach (var link in grid.links) {
            Route route = CreateRouteByLink(link);
            solution.Add(route);
        }
        routes = new Route[grid.links.Length];
        if (linePrefab != null) {
            int i = lines.childCount;
            for (; i < grid.links.Length; ++i) {
                Transform line = Instantiate(linePrefab).transform;
                line.SetParent(lines, false);
            }
            for (i = grid.links.Length; i < lines.childCount; ++i) {
                lines.GetChild(i).gameObject.SetActive(false);
            }
            lineRenderers = new LineRenderer[grid.links.Length];
            for (i = 0; i < grid.links.Length; ++i) {
                lines.GetChild(i).gameObject.SetActive(true);
                lineRenderers[i] = lines.GetChild(i).GetComponent<LineRenderer>();
                lineRenderers[i].material.color = GameManager.instance.GetContentColor(grid.links[i].pair.contentId);
                lineRenderers[i].positionCount = 0;
            }
        }
        currentWord = string.Empty;
        if (onCurrentWordChange != null) {
            onCurrentWordChange.Invoke(currentWord);
        }
        steps = 0;
        if (onStepsChange != null) {
            onStepsChange.Invoke(steps);
        }
    }

    public void LoadNextLevel() {
        NextLevel();
    }

    public void LoadPrevLevel() {
        PrevLevel();
    }

    public bool NextLevel() {
        if (GameManager.instance.NextLevel()) {
            Load(GameManager.instance.currentLevel);
            return true;
        }
        GameManager.instance.SwitchScene("Menu");
        return false;
    }

    public bool PrevLevel() {
        if (GameManager.instance.PrevLevel()) {
            Load(GameManager.instance.currentLevel);
            return true;
        }
        GameManager.instance.SwitchScene("Menu");
        return false;
    }

    public string GetChar(int contentId) {
        int index = charIndices[contentId - 1];
        if (index >= characters.Length) return string.Empty;
        return characters[index].ToString();
    }

    public int GetId(char ch) {
        if (charToIds.ContainsKey(ch)) {
            return charToIds[ch];
        }
        return 0;
    }

    public Color GetColor(char ch) {
        int id = GetId(ch);
        if (id <= 0) return Color.black.NewA(0);
        return GameManager.instance.contentColor[id - 1];
    }

    public char[] GetCharArray() {
        return currentWord.ToCharArray();
    }

    private Route CreateRouteByLink(Link link) {
        Cell start = cells[link.pair.startCoords.y, link.pair.startCoords.x];
        Cell end = cells[link.pair.endCoords.y, link.pair.endCoords.x];
        start.contentId = link.pair.contentId;
        end.contentId = link.pair.contentId;
        Route route = new Route();
        route.AddLast(start);
        for (int i = 0; i < link.flow.coordsList.Count; ++i) {
            Cell cell = cells[link.flow.coordsList[i].y, link.flow.coordsList[i].x];
            route.AddLast(cell);
        }
        route.AddLast(end);
        return route;
    }

    public void Save() {
        if (grid.links == null) {
            Debug.LogWarning("This level has not been solved, please solve first!");
            return;
        }
        if (grid.word.Length != grid.links.Length) {
            Debug.LogWarning("Word length must be equal with the flow count!");
            return;
        }
        SaveHelper.SaveData(levelFilePath, grid);
        level = GameManager.instance.GetNextLevel(level);
        if (onLevelChange != null) {
            onLevelChange.Invoke(level);
        }
    }

    public void Reset() {
        for (int i = 0; i < routes.Length; ++i) {
            if (routes[i] != null) {
                routes[i].Reset();
                routes[i] = null;
            }
        }
        currentWord = string.Empty;
        if (onCurrentWordChange != null) {
            onCurrentWordChange.Invoke(currentWord);
        }
        steps = 0;
        if (onStepsChange != null) {
            onStepsChange.Invoke(steps);
        }
    }

    public void Hint() {
        Route route = null;
        int index = 0;
        foreach (int i in grid.indices) {
            if (routes[i] == null || !routes[i].IsSameTo(solution[i])) {
                route = solution[i];
                index = i;
                break;
            }
        }
        if (route != null) {
            if (routes[index] != null) {
                routes[index].Unhighlight();
            }
            routes[index] = route.Copy();
            routes[index].Highlight();
            Validate(routes[index]);
            foreach (var r in routes) {
                if (r == null) continue;
                string c = GetChar(r.First.Value.contentId);
                if (!r.IsValid) {
                    currentWord = currentWord.Replace(c, "");
                } else if (r == routes[index]) {
                    if (r.IsValid && !currentWord.Contains(c)) {
                        currentWord += c;
                    }
                    ++steps;
                    if (onStepsChange != null) {
                        onStepsChange.Invoke(steps);
                    }
                    GameManager.instance.PlaySound("link");
                }
            }
            if (onCurrentWordChange != null) {
                onCurrentWordChange.Invoke(currentWord);
            }
            if (won) {
                Win();
            }
        }
    }

    private const float winAnimInterval = 0.1f;
    private const float winAnimDuration = 0.25f;

    public void Win1() {
        over = true;
        if (onInteractableChange != null) {
            onInteractableChange.Invoke(over);
        }
        int maxLength = 0;
        foreach (var route in routes) {
            maxLength = Mathf.Max(maxLength, route.Count);
            route.Win(winAnimInterval, winAnimDuration);
        }
        Invoke("Finish", maxLength * winAnimInterval + winAnimDuration);
    }

    public void Win() {
        GameManager.instance.PlaySound("win");
        over = true;
        if (onInteractableChange != null) {
            onInteractableChange.Invoke(over);
        }
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                cells[i, j].Win((i + j) * winAnimInterval, winAnimDuration);
            }
        }
        Invoke("Finish", (rows + cols) * winAnimInterval + winAnimDuration * 2);
    }

    void Finish() {
        over = false;
        if (onInteractableChange != null) {
            onInteractableChange.Invoke(over);
        }
        GameController.GetInstance().LevelComplete(stars);
    }

    public void Validate(Route route) {
        if (route.Count < 2) return;
        for (var node = route.First; node != null; node = node.Next) {
            if (node.Value.lastId > 0) {
                routes[node.Value.lastId - 1].BreakFrom(node.Value);
                node.Value.lastId = 0;
            }
        }
    }

    public void SetRouteDirty(int contentId) {
        if (routes[contentId - 1] != null) {
            routes[contentId - 1].Dirty = true;
        }
    }

    private Cell GetNextCell(Cell cell, Direction direction) {
        switch (direction) {
            case Direction.Up:
                return cell.row < rows - 1 ? cells[cell.row + 1, cell.col] : null;
            case Direction.Down:
                return cell.row > 0 ? cells[cell.row - 1, cell.col] : null;
            case Direction.Left:
                return cell.col > 0 ? cells[cell.row, cell.col - 1] : null;
            case Direction.Right:
                return cell.col < cols - 1 ? cells[cell.row, cell.col + 1] : null;
            default:
                throw new System.InvalidOperationException("Unknown direction");
        }
    }

    private Direction[] GetSearchDirection(Cell source, Cell target) {
        List<Direction> directions = new List<Direction>();
        if (target.col > source.col) {
            directions.Add(Direction.Right);
            if (target.row > source.row) {
                directions.Add(Direction.Up);
            } else {
                directions.Add(Direction.Down);
            }
        } else if (target.col < source.col) {
            directions.Add(Direction.Left);
            if (target.row < source.row) {
                directions.Add(Direction.Down);
            } else {
                directions.Add(Direction.Up);
            }
        } else if (target.row > source.row) {
            directions.Add(Direction.Up);
        } else if (target.row < source.row) {
            directions.Add(Direction.Down);
        }
        return directions.ToArray();
    }

    public Route Link(Cell source, Cell target) {
        List<Route> routes = new List<Route>();
        foreach (var direction in GetSearchDirection(source, target)) {
            Route route = new Route();
            route.AddLast(source);
            Link(routes, route, target, direction);
        }
        Route shortest = null;
        for (int i = 0; i < routes.Count; ++i) {
            if (shortest == null || routes[i].Count < shortest.Count) {
                shortest = routes[i];
                if (shortest.Count == 2) {
                    break;
                }
            }
        }
        return shortest;
    }

    private void Link(List<Route> routes, Route route, Cell target, Direction direction) {
        Cell next = GetNextCell(route.Last.Value, direction);
        if (next == target) {
            route.AddLast(target);
            routes.Add(route);
            return;
        }
        if (next == null || next.contentId > 0 || next.currentId > 0 || route.Contains(next)) {
            return;
        }
        route.AddLast(next);
        foreach (var directionToTry in GetSearchDirection(next, target)) {
            Link(routes, route.Copy(), target, directionToTry);
        }
    }

    public void OnPointerDown(PointerEventData eventData) {
        if (!editable && !over && eventData.pointerCurrentRaycast.gameObject != null) {
            Cell cell = eventData.pointerCurrentRaycast.gameObject.GetComponentInParent<Cell>();
            if (cell != null) {
                int selectId = 0; ;
                if (cell.contentId > 0) {
                    selectId = cell.contentId;
                    curRoute = routes[cell.contentId - 1];
                    if (curRoute != null && curRoute.First != null) {
                        curRoute.Reset();
                    }
                    cell.Highlight();
                    if (curRoute == null) {
                        curRoute = new Route();
                        routes[cell.contentId - 1] = curRoute;
                    }
                    curRoute.AddFirst(cell);
                } else if (cell.currentId > 0 && 
                    (routes[cell.currentId - 1].First.Value == cell ||
                     routes[cell.currentId - 1].Last.Value == cell)) {
                    selectId = cell.currentId;
                    curRoute = routes[cell.currentId - 1];
                }
                if (selectId > 0) {
                    solution[selectId - 1].First.Value.OnSelect();
                    solution[selectId - 1].Last.Value.OnSelect();
                }
                if (cursor != null && curRoute != null) {
                    GameManager.instance.PlaySound("touch");
                    cursor.gameObject.SetActive(true);
                    cursorRenderer.color = cursorRenderer.color.NewRGB(GameManager.instance.GetContentColor(curRoute.First.Value.contentId));
                    cursor.position = Camera.main.ScreenToWorldPoint(eventData.position).NewZ(0);
                }
            }
        }
    }

    public void OnDrag(PointerEventData eventData) {
        if (!editable && !over && eventData.pointerCurrentRaycast.gameObject != null && curRoute != null) {
            Cell cell = eventData.pointerCurrentRaycast.gameObject.GetComponentInParent<Cell>();
            if (cell != null) {
                var node = curRoute.FindLast(cell);
                if (node != null) {
                    curRoute.BreakAt(node);
                } else if (curRoute.Count < 2 || curRoute.Last.Value.contentId == 0) {
                    var route = Link(curRoute.Last.Value, cell);
                    if (route != null) {
                        for (var n = route.First.Next; n != null; n = n.Next) {
                            if (n.Value.contentId == 0 || n.Value.contentId == curRoute.First.Value.contentId) {
                                if (n.Value.currentId > 0 && n.Value.currentId != curRoute.First.Value.contentId) {
                                    GameManager.instance.PlaySound("break");
                                }
                                n.Value.Highlight(curRoute.First.Value.contentId);
                                curRoute.AddLast(n.Value);
                            }
                        }
                    }
                }
            }
        }
        if (cursor != null && curRoute != null) {
            cursor.gameObject.SetActive(true);
            cursor.position = Camera.main.ScreenToWorldPoint(eventData.position).NewZ(0);
        }
    }

    public void OnPointerUp(PointerEventData eventData) {
        if (!editable && !over && curRoute != null) {
            Validate(curRoute);
            if (cursor != null) {
                cursor.gameObject.SetActive(false);
            }
            foreach (var route in routes) {
                if (route == null) continue;
                string c = GetChar(route.First.Value.contentId);
                if (!route.IsValid) {
                    currentWord = currentWord.Replace(c, "");
                } else if (route == curRoute) {
                    if (!currentWord.Contains(c)) {
                        currentWord += c;
                    }
                    ++steps;
                    if (onStepsChange != null) {
                        onStepsChange.Invoke(steps);
                    }
                    GameManager.instance.PlaySound("link");
                }
            }
            if (onCurrentWordChange != null) {
                onCurrentWordChange.Invoke(currentWord);
            }
            curRoute = null;
            if (won) {
                Win();
            }
        }
    }

    public void OnPointerClick(PointerEventData eventData) {
        if (editable && eventData.pointerCurrentRaycast.gameObject != null) {
            Cell cell = eventData.pointerCurrentRaycast.gameObject.GetComponentInParent<Cell>();
            if (cell != null) {
                if (eventData.button == PointerEventData.InputButton.Left) {
                    cell.contentId = selectedContentId;
                    ManualSetContentId(cell.col, cell.row, cell.contentId);
                } else if (eventData.button == PointerEventData.InputButton.Right && cell.contentId > 0) {
                    ChangeSelectContentId(cell.contentId);
                    cell.contentId = 0;
                    ManualSetContentId(cell.col, cell.row, cell.contentId);
                }
            }
            grid.PrintCells();
        }
    }

}
