﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HintButton : MonoBehaviour {

    private Text count;

	// Use this for initialization
	void Start() {
        count = GetComponentInChildren<Text>();
        GetComponent<Button>().onClick.AddListener(Hint);
        Update();
    }
	
	// Update is called once per frame
	void Update() {
        count.text = GameManager.instance.hints.ToString();
    }

    void Hint() {
        if (GameManager.instance.hints > 0) {
            Board.GetInstance().Hint();
            --GameManager.instance.hints;
        } else if (EzAds.IsRewardedVideoLoaded()) {
            Panel.Open("GetHintsPanel");
        }
    }
}
