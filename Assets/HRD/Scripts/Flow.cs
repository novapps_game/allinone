﻿using System;
using System.Linq;
using System.Collections.Generic;

[System.Serializable]
public class Flow {

    public List<Coords> coordsList = new List<Coords>();

    public Flow Copy() {
        var copy = new Flow();
        foreach (var coords in coordsList) {
            copy.AddCoords(coords);
        }
        return copy;
    }

    public void AddCoords(Coords coords) {
        coordsList.Add(coords);
    }

    public bool ContainsCoords(Coords coords) {
        return coordsList.Any(c => c.Equals(coords));
    }

    public Coords GetNextCoords(Direction direction) {
        var lastCoords = coordsList.Last();
        switch (direction) {
            case Direction.Up:
                return new Coords(lastCoords.x, lastCoords.y + 1);
            case Direction.Down:
                return new Coords(lastCoords.x, lastCoords.y - 1);
            case Direction.Left:
                return new Coords(lastCoords.x - 1, lastCoords.y);
            case Direction.Right:
                return new Coords(lastCoords.x + 1, lastCoords.y);
            default:
                throw new InvalidOperationException("Unknown direction");
        }
    }

}
