﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class LevelGroupItem : ScrollListItem {

    protected override void OnInit() {
        base.OnInit();
        var group = GameManager.instance.levelGroups[index];
        name = group.name;
        string[] sizes = name.Split('x');
        transform.Find("Size/Cols").GetComponent<Text>().text = sizes[0];
        transform.Find("Size/Rows").GetComponent<Text>().text = sizes[1];
        Transform levels = transform.Find("Levels");
        GameObject prefab = levels.GetChild(0).gameObject;
        int levelCount = group.count;
        for (int i = levels.childCount; i < levelCount; ++i) {
            LevelButton levelButton = Instantiate(prefab).GetComponent<LevelButton>();
            levelButton.transform.SetParent(levels, false);
            levelButton.id = i + 1;
        }
    }

}
