﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stars : MonoBehaviour {

    public float gainInterval = 0.5f;

	// Use this for initialization
	void Start() {
        int stars = Board.GetInstance().stars;
        for (int i = 0; i < stars; ++i) {
            iTween.FadeTo(transform.GetChild(i).gameObject, iTween.Hash("alpha", 1, "time", 0.1f,
                "delay", gainInterval * i, "easetype", iTween.EaseType.linear, 
                "onstart", "OnGain", "onstarttarget", gameObject, "onstartparams", i));
        }
	}
	
	void OnGain(int index) {
        GameManager.instance.PlaySound("star");
        GainStarSpawner.Spawn(transform.GetChild(index).position);
	}
}
