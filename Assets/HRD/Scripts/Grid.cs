﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using DlxLib;

[System.Serializable]
public class Grid {

    public int width;
    public int height;
    public Link[] links;
    public string word = string.Empty;
    public int[] indices;

    private Pair[] pairs;
    private int[,] cells;
    private int[,] solution;

    public Grid(int width, int height) {
        this.width = width;
        this.height = height;
        //cells = new int[this.height, this.width];
        solution = new int[this.height, this.width];
    }

    public Grid(int width, int height, params Pair[] pairs) {
        this.width = width;
        this.height = height;
        this.pairs = pairs;
        cells = new int[this.height, this.width];
        solution = new int[this.height, this.width];
        foreach (var pair in pairs) {
            cells[pair.startCoords.y, pair.startCoords.x] = pair.contentId;
            cells[pair.endCoords.y, pair.endCoords.x] = pair.contentId;
        }
        Solve();
    }

    public Grid(int width, int height, params Link[] links) {
        this.width = width;
        this.height = height;
        this.links = links;
        Fill();
    }

    public void Fill() {
        pairs = new Pair[links.Length];
        cells = new int[height, width];
        solution = new int[height, width];
        for (int i = 0; i < links.Length; ++i) {
            Link link = links[i];
            Pair pair = link.pair;
            Flow flow = link.flow;
            var contentId = pair.contentId;
            pairs[i] = pair;
            cells[pair.startCoords.y, pair.startCoords.x] = contentId;
            cells[pair.endCoords.y, pair.endCoords.x] = contentId;
            foreach (var coords in flow.coordsList) {
                solution[coords.y, coords.x] = contentId;
            }
        }
    }

    public int Generate() {
        cells = new int[this.height, this.width];
        Tile();
        Shuffle();
        OddCorner();
        FindFlows();
        return Flatten();
    }

    public bool HasTwin() {
        foreach (var link in links) {
            if (link.flow.coordsList.Count == 0) {
                return true;
            }
        }
        return false;
    }

    private void Tile() {
        int id = 1;
        for (int y = 0; y < height -1; y += 2) {
            for (int x = 0; x < width; ++x) {
                solution[y, x] = id;
                solution[y + 1, x] = id;
                ++id;
            }
        }
        // Add padding in case of odd height
        if (height % 2 == 1) {
            for (int x = 0; x < width - 1; x += 2) {
                solution[height - 1, x] = id;
                solution[height - 1, x + 1] = id;
                ++id;
            }
            // In case of odd width, add a single in the corner.
            // We will merge it into a real flow after shuffeling
            if (width % 2 == 1) {
                solution[height - 1, width - 1] = id;
            }
        }
    }

    private void Shuffle() {
        int count = width * height * width * height;
        for (int k = 0; k < count; ++k) {
            int x = Random.Range(0, width - 1);
            int y = Random.Range(0, height - 1);
            if (solution[y, x] == solution[y, x + 1] && solution[y + 1, x] == solution[y + 1, x + 1]) {
                // Horizontal case
                // aa \ ab
                // bb / ab
                solution[y + 1, x] = solution[y, x];
                solution[y, x + 1] = solution[y + 1, x + 1];
            } else if (solution[y, x] == solution[y + 1, x] && solution[y, x + 1] == solution[y + 1, x + 1]) {
                // Vertical case
                // ab \ aa
                // ab / bb
                solution[y, x + 1] = solution[y, x];
                solution[y + 1, x] = solution[y + 1, x + 1];
            }
        }
    }

    private void OddCorner() {
        if (width % 2 == 1 && height % 2 == 1) {
            if (width > 2 && solution[height - 1, width - 3] == solution[height - 1, width - 2]) {
                solution[height - 1, width - 1] = solution[height - 1, width - 2];
            }
            if (height > 2 && solution[height - 3, width - 1] == solution[height - 2, width - 1]) {
                solution[height - 1, width - 1] = solution[height - 2, width - 1];
            }
        }
    }

    private void FindFlows() {
        foreach (var k in EzUtil.RandomPerm(0, width * height)) {
            int x = k % width;
            int y = k / width;
            if (IsFlowHead(x, y)) {
                LayFlow(x, y);
            }
        }
    }

    private static readonly int[] DX = {  0, 1, 0, -1 };
    private static readonly int[] DY = { -1, 0, 1,  0 };

    private bool IsFlowHead(int x, int y) {
        int links = 0;
        for (int i = 0; i < 4; ++i) {
            int x1 = x + DX[i];
            int y1 = y + DY[i];
            if (Contains(x1, y1) && solution[y1, x1] == solution[y, x]) {
                ++links;
            }
        }
        return links < 2;
    }

    private void LayFlow(int x, int y) {
        foreach (var i in EzUtil.RandomPerm(0, 4)) {
            int x1 = x + DX[i];
            int y1 = y + DY[i];
            if (Contains(x1, y1) && CanLink(x, y, x1, y1)) {
                Spread(x1, y1, solution[y, x]);
            }
        }
    }

    public bool CanLink(int x1, int y1, int x2, int y2) {
        if (solution[y1, x1] == solution[y2, x2]) {
            return false;
        }
        if (!IsFlowHead(x1, y1) || !IsFlowHead(x2, y2)) {
            return false;
        }
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                for (int i = 0; i < 4; ++i) {
                    int x0 = x + DX[i];
                    int y0 = y + DY[i];
                    if (Contains(x0, y0) && !(x == x1 && y == y1 && x0 == x2 && y0 == y2) && 
                        solution[y, x] == solution[y1, x1] && solution[y0, x0] == solution[y2, x2]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private void Spread(int x, int y, int contentId) {
        int origin = solution[y, x];
        solution[y, x] = contentId;
        for (int i = 0; i < 4; ++i) {
            int x1 = x + DX[i];
            int y1 = y + DY[i];
            if (Contains(x1, y1) && solution[y1, x1] == origin) {
                Spread(x1, y1, contentId);
            }
        }
    }

    private int Flatten() {
        int contentId = -1;
        int x, y;
        for (y = 0; y < height; ++y) {
            for (x = 0; x < width; ++x) {
                if (solution[y, x] > 0) {
                    Spread(x, y, contentId);
                    --contentId;
                }
            }
        }
        for (y = 0; y < height; ++y) {
            for (x = 0; x < width; ++x) {
                solution[y, x] = -solution[y, x];
            }
        }
        int flows = -contentId - 1;
        pairs = new Pair[flows];
        links = new Link[flows];
        for (y = 0; y < height; ++y) {
            for (x = 0; x < width; ++x) {
                contentId = solution[y, x];
                if (contentId > 0) {
                    Pair pair = pairs[contentId - 1];
                    if (pair == null) {
                        pair = new Pair(contentId);
                        pairs[contentId - 1] = pair;
                        links[contentId - 1] = new Link(pair);
                    }
                    Coords coords = new Coords(x, y);
                    if (IsFlowHead(x, y)) {
                        if (pair.startCoords == null) {
                            pair.startCoords = coords;
                        } else if (pair.endCoords == null) {
                            pair.endCoords = coords;
                        }
                        cells[y, x] = contentId;
                    } else {
                        links[contentId - 1].flow.AddCoords(coords);
                    }
                }
            }
        }
        FixLinks();
        indices = EzUtil.RandomPerm(0, flows);
        return flows;
    }

    private void FixLinks() {
        foreach (var link in links) {
            List<Coords> coordsList = new List<Coords>();
            Coords last = link.pair.startCoords;
            while (link.flow.coordsList.Count > 0) {
                for (int i = 0; i < link.flow.coordsList.Count; ++i) {
                    Coords coords = link.flow.coordsList[i];
                    if (last.IsNeighbor(coords)) {
                        coordsList.Add(coords);
                        last = coords;
                        link.flow.coordsList.RemoveAt(i);
                        break;
                    }
                }
            }
            link.flow.coordsList = coordsList;
        }
    }

    public bool Contains(int x, int y) {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    public bool Contains(Coords coords) {
        return Contains(coords.y, coords.x);
    }

    public bool IsCellOccupied(Coords coords) {
        return cells[coords.y, coords.x] > 0;
    }

    public int GetContentIdAtCoords(Coords coords) {
        return solution[coords.y, coords.x];
    }

    public void ManualSetContentId(int x, int y, int id) {
        cells[y, x] = id;
        pairs = null;
        links = null;
    }

    private Pair[] Parse() {
        int maxId = 0;
        int x, y;
        for (y = 0; y < height; ++y) {
            for (x = 0; x < width; ++x) {
                maxId = Mathf.Max(maxId, cells[y, x]);
            }
        }
        Pair[] prs = new Pair[maxId];
        for (y = 0; y < height; ++y) {
            for (x = 0; x < width; ++x) {
                int id = cells[y, x];
                if (id > 0) {
                    Pair pair = prs[id - 1];
                    if (pair == null) {
                        pair = new Pair(id);
                        prs[id - 1] = pair;
                    }
                    if (pair.startCoords == null) {
                        pair.startCoords = new Coords(x, y);
                    } else if (pair.endCoords == null) {
                        pair.endCoords = new Coords(x, y);
                    } else {
                        Debug.LogWarning("There are more then 2 of id(" + id + ")");
                        return null;
                    }
                }
            }
        }
        for (int i = 0; i < prs.Length; ++i) {
            if (prs[i] == null) {
                Debug.LogWarning("Id(" + (i + 1) + ") not found in pairs");
                return null;
            }
        }
        return prs;
    }

    public bool Solve() {
        if (links != null) {
            Debug.Log("Has been solved.");
            return true;
        }
        if (pairs == null) {
            pairs = Parse();
            if (pairs == null) {
                Debug.LogWarning("Invalid grid! Cannot solve it!");
                return false;
            }
        }
        Dictionary<int, Link> rowIndexToLink = new Dictionary<int, Link>();
        var matrix = BuildMatrix(rowIndexToLink);
        var dlx = new Dlx();
        var solutions = dlx.Solve(matrix);
        foreach (var solution in solutions) {
            links = solution.RowIndexes.Select((index) => { return rowIndexToLink[index]; }).ToArray();
            break; // need only one solution
        }
        if (links == null) {
            Debug.LogWarning("No solution found!");
            return false;
        }
        foreach (var link in links) {
            foreach (var coords in link.flow.coordsList) {
                solution[coords.y, coords.x] = link.pair.contentId;
            }
            link.flow.coordsList.Remove(link.pair.startCoords);
            link.flow.coordsList.Remove(link.pair.endCoords);
        }
        Debug.Log("Solve successfully!");
        indices = EzUtil.RandomPerm(0, links.Length);
        return true;
    }

    private bool[,] BuildMatrix(Dictionary<int, Link> rowIndexToLink) {
        int columns = pairs.Length + (width * height);
        var internalData = new List<IList<bool>>();
        pairs.Select((pair, pairIndex) => {
            AddInternalDataRowsForPair(internalData, rowIndexToLink, pair, pairIndex);
            return 0;
        }).ToList();
        var matrix = new bool[internalData.Count, columns];
        for (var row = 0; row < internalData.Count; row++) {
            for (var col = 0; col < columns; col++) {
                matrix[row, col] = internalData[row][col];
            }
        }
        return matrix;
    }

    private void AddInternalDataRowsForPair(List<IList<bool>> internalData, 
        Dictionary<int, Link> rowIndexToLink, Pair pair, int pairIndex) {
        var flows = FindAllFlows(pair.startCoords, pair.endCoords);
        foreach (var flow in flows.FlowList) {
            var internalDataRow = BuildInternalDataRowForColorPairPath(pairIndex, flow);
            internalData.Add(internalDataRow);
            var rowIndex = internalData.Count - 1;
            rowIndexToLink.Add(rowIndex, new Link(pair, flow));
        }
    }

    private IList<bool> BuildInternalDataRowForColorPairPath(int pairIndex, Flow flow) {
        int columns = pairs.Length + (width * height);
        var internalDataRow = new bool[columns];
        internalDataRow[pairIndex] = true;
        foreach (var coords in flow.coordsList) {
            var gridLocationColumnIndex = pairs.Length + (width * coords.x) + coords.y;
            internalDataRow[gridLocationColumnIndex] = true;
        }
        return internalDataRow;
    }

    private Flows FindAllFlows(Coords startCoords, Coords endCoords) {
        var flows = new Flows();
        FollowFlow(flows, startCoords.StartPath(), endCoords, Direction.Up, 0);
        FollowFlow(flows, startCoords.StartPath(), endCoords, Direction.Down, 0);
        FollowFlow(flows, startCoords.StartPath(), endCoords, Direction.Left, 0);
        FollowFlow(flows, startCoords.StartPath(), endCoords, Direction.Right, 0);
        return flows;
    }

    private void FollowFlow(Flows flows, Flow currentFlow, Coords endCoords, Direction direction, int numDirectionChanges) {
        var nextCoords = currentFlow.GetNextCoords(direction);
        if (nextCoords.Equals(endCoords)) {
            currentFlow.AddCoords(nextCoords);
            flows.AddFlow(currentFlow);
            return;
        }
        if (!Contains(nextCoords) || currentFlow.ContainsCoords(nextCoords) || IsCellOccupied(nextCoords)) {
            return;
        }
        currentFlow.AddCoords(nextCoords);

        var allDirections = System.Enum.GetValues(typeof(Direction)).Cast<Direction>();
        var oppositeDirection = direction.Opposite();
        var directionsToTry = allDirections.Where(d => d != oppositeDirection);

        var numEmptyCells = width * height - pairs.Length * 2;
        var maxDirectionChanges = Mathf.Max(1, numEmptyCells / 5);

        foreach (var directionToTry in directionsToTry) {
            var newNumDirectionChanges = numDirectionChanges + (directionToTry != direction ? 1 : 0);
            if (newNumDirectionChanges <= maxDirectionChanges) {
                FollowFlow(flows, currentFlow.Copy(), endCoords, directionToTry, newNumDirectionChanges);
            }
        }
    }

    public void PrintCells() {
        Print(cells);
    }

    public void PrintSolution() {
        Print(solution);
    }

    private void Print(int[,] table) {
        for (var y = height - 1; y >= 0; y--) {
            // The inner loop iterates over the columns.
            string row = string.Empty;
            for (var x = 0; x < width; x++) {
                var id = table[y, x];
                if (id > 0) {
                    row += "\t" + id;
                } else {
                    row += "\t";
                }
            }
            Debug.Log(row);
        }
    }
}
