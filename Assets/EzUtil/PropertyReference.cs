//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2016 Tasharen Entertainment
//----------------------------------------------

using System.Reflection;
using System.Diagnostics;
using UnityEngine;
using System;

/// <summary>
/// Reference to a specific field or property that can be set via inspector.
/// </summary>
[System.Serializable]
public class PropertyReference {
    [SerializeField] Component target;
    [SerializeField] string name;

    FieldInfo field = null;
    PropertyInfo property = null;

    /// <summary>
    /// Event delegate's target object.
    /// </summary>
    public Component Target {
        get {
            return target;
        }
        set {
            target = value;
            property = null;
            field = null;
        }
    }

    /// <summary>
    /// Event delegate's method name.
    /// </summary>
    public string Name {
        get {
            return name;
        }
        set {
            name = value;
            property = null;
            field = null;
        }
    }

    /// <summary>
    /// Whether this delegate's values have been set.
    /// </summary>
    public bool Valid { get { return (target != null && !string.IsNullOrEmpty(name)); } }

    /// <summary>
    /// Whether the target script is actually enabled.
    /// </summary>
    public bool Enabled {
        get {
            if (target == null) return false;
            MonoBehaviour mb = (target as MonoBehaviour);
            return (mb == null || mb.enabled);
        }
    }

    public PropertyReference() { }
    public PropertyReference(Component target, string fieldName) {
        this.target = target;
        name = fieldName;
    }

    /// <summary>
    /// Helper function that returns the property type.
    /// </summary>
    public Type GetPropertyType() {
        if (property == null && field == null && Valid) Cache();
        if (property != null) return property.PropertyType;
        if (field != null) return field.FieldType;
#if UNITY_EDITOR || !UNITY_FLASH
        return typeof(void);
#else
		return null;
#endif
    }

    /// <summary>
    /// Equality operator.
    /// </summary>
    public override bool Equals(object obj) {
        if (obj == null) {
            return !Valid;
        }
        if (obj is PropertyReference) {
            PropertyReference pb = obj as PropertyReference;
            return (target == pb.target && string.Equals(name, pb.name));
        }
        return false;
    }

    static int s_Hash = "PropertyBinding".GetHashCode();

    /// <summary>
    /// Used in equality operators.
    /// </summary>
    public override int GetHashCode() { return s_Hash; }

    /// <summary>
    /// Set the delegate callback using the target and method names.
    /// </summary>
    public void Set(Component target, string methodName) {
        this.target = target;
        name = methodName;
    }

    /// <summary>
    /// Clear the event delegate.
    /// </summary>
    public void Clear() {
        target = null;
        name = null;
    }

    /// <summary>
    /// Reset the cached references.
    /// </summary>
    public void Reset() {
        field = null;
        property = null;
    }

    /// <summary>
    /// Convert the delegate to its string representation.
    /// </summary>
    public override string ToString() { return ToString(target, Name); }

    /// <summary>
    /// Convenience function that converts the specified component + property pair into its string representation.
    /// </summary>
    public static string ToString(Component comp, string property) {
        if (comp != null) {
            string typeName = comp.GetType().ToString();
            int period = typeName.LastIndexOf('.');
            if (period > 0) typeName = typeName.Substring(period + 1);

            if (!string.IsNullOrEmpty(property)) return typeName + "." + property;
            else return typeName + ".[property]";
        }
        return null;
    }

    /// <summary>
    /// Retrieve the property's value.
    /// </summary>
    [DebuggerHidden]
    [DebuggerStepThrough]
    public object Get() {
        if (property == null && field == null && Valid) Cache();
        try {
            if (property != null) {
                if (property.CanRead)
                    return property.GetValue(target, null);
            } else if (field != null) {
                return field.GetValue(target);
            }
        } catch (Exception) {
            return null;
        }
        return null;
    }

    /// <summary>
    /// Assign the bound property's value.
    /// </summary>

    [DebuggerHidden]
    [DebuggerStepThrough]
    public bool Set(object value) {
        if (property == null && field == null && Valid) Cache();
        if (property == null && field == null) return false;

        if (value == null) {
            try {
                if (property != null) {
                    if (property.CanWrite) {
                        property.SetValue(target, null, null);
                        return true;
                    }
                } else {
                    field.SetValue(target, null);
                    return true;
                }
            } catch (Exception) {
                return false;
            }
        }

        // Can we set the value?
        if (!Convert(ref value)) {
            if (Application.isPlaying)
                UnityEngine.Debug.LogError("Unable to convert " + value.GetType() + " to " + GetPropertyType());
        } else if (field != null) {
            field.SetValue(target, value);
            return true;
        } else if (property.CanWrite) {
            property.SetValue(target, value, null);
            return true;
        }
        return false;
    }

    /// <summary>
    /// Cache the field or property.
    /// </summary>
    [DebuggerHidden]
    [DebuggerStepThrough]
    bool Cache() {
        if (target != null && !string.IsNullOrEmpty(name)) {
            Type type = target.GetType();
            field = type.GetFieldEx(name);
            property = type.GetPropertyEx(name);
        } else {
            field = null;
            property = null;
        }
        return (field != null || property != null);
    }

    /// <summary>
    /// Whether we can assign the property using the specified value.
    /// </summary>
    bool Convert(ref object value) {
        if (target == null) return false;

        Type to = GetPropertyType();
        Type from;

        if (value == null) {
#if NETFX_CORE
			if (!to.GetTypeInfo().IsClass) return false;
#else
            if (!to.IsClass) return false;
#endif
            from = to;
        } else from = value.GetType();
        return Convert(ref value, from, to);
    }

    /// <summary>
    /// Whether we can convert one type to another for assignment purposes.
    /// </summary>
    public static bool Convert(Type from, Type to) {
        object temp = null;
        return Convert(ref temp, from, to);
    }

    /// <summary>
    /// Whether we can convert one type to another for assignment purposes.
    /// </summary>
    public static bool Convert(object value, Type to) {
        if (value == null) {
            value = null;
            return Convert(ref value, to, to);
        }
        return Convert(ref value, value.GetType(), to);
    }

    /// <summary>
    /// Whether we can convert one type to another for assignment purposes.
    /// </summary>
    public static bool Convert(ref object value, Type from, Type to) {
#if REFLECTION_SUPPORT
        // If the value can be assigned as-is, we're done
#if NETFX_CORE
		if (to.GetTypeInfo().IsAssignableFrom(from.GetTypeInfo())) return true;
#else
        if (to.IsAssignableFrom(from)) return true;
#endif

#else
		if (from == to) return true;
#endif
        // If the target type is a string, just convert the value
        if (to == typeof(string)) {
            value = (value != null) ? value.ToString() : "null";
            return true;
        }

        // If the value is null we should not proceed further
        if (value == null) return false;

        if (to == typeof(int)) {
            if (from == typeof(string)) {
                int val;

                if (int.TryParse((string)value, out val)) {
                    value = val;
                    return true;
                }
            } else if (from == typeof(float)) {
                value = Mathf.RoundToInt((float)value);
                return true;
            } else if (from == typeof(double)) {
                value = (int)Math.Round((double)value);
            }
        } else if (to == typeof(float)) {
            if (from == typeof(string)) {
                float val;

                if (float.TryParse((string)value, out val)) {
                    value = val;
                    return true;
                }
            } else if (from == typeof(double)) {
                value = (float)(double)value;
            } else if (from == typeof(int)) {
                value = (float)(int)value;
            }
        } else if (to == typeof(double)) {
            if (from == typeof(string)) {
                double val;

                if (double.TryParse((string)value, out val)) {
                    value = val;
                    return true;
                }
            } else if (from == typeof(float)) {
                value = (double)(float)value;
            } else if (from == typeof(int)) {
                value = (double)(int)value;
            }
        }
        return false;
    }
}
