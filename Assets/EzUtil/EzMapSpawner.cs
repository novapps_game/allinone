﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

public class EzMapSpawner : EzTransformPools {

    public bool moveLeft = true;
    public bool moveRight = true;
    public bool moveUnder = true;
    public bool moveUpper = true;

    public int randomMinIndex;
    public List<int> mapIndices;
    protected virtual EzTransformPool randomPool {
        get {
            if (mapIndices.Count > 0) {
                int index = mapIndices[0];
                mapIndices.RemoveAt(0);
                return pools[index];
            }
            return pools[Random.Range(randomMinIndex, pools.Length)];
        }
    }

    public EzMap currentMap;
    public float parallax = 0f;
    public float smooth = 0f;

    private Vector3 lastCameraPos;

    // Use this for initialization
    protected virtual void Start() {
        currentMap = CreateMap();
        lastCameraPos = Camera.main.transform.position;
    }

    // Update is called once per frame
    protected virtual void Update() {
        if (parallax != 0) {
            MoveParallax();
        }
        if (moveLeft) {
            MoveLeft();
        }
        if (moveRight) {
            MoveRight();
        }
        if (moveUnder) {
            MoveUnder();
        }
        if (moveUpper) {
            MoveUpper();
        }
    }

    protected virtual void MoveParallax() {
        Vector2 offset = Camera.main.transform.position - lastCameraPos;
        if (smooth > 0) {
            transform.localPosition = Vector3.Lerp(transform.localPosition, offset.ToV3() * parallax, smooth * Time.deltaTime);
        } else {
            transform.localPosition += offset.ToV3() * parallax;
        }
        lastCameraPos = Camera.main.transform.position;
    }

    protected virtual void MoveLeft() {
        if (currentMap.upper != null) currentMap.upper.MoveLeft();
        if (currentMap.under != null) currentMap.under.MoveLeft();
        currentMap = currentMap.MoveLeft();
    }

    protected virtual void MoveRight() {
        if (currentMap.upper != null) currentMap.upper.MoveRight();
        if (currentMap.under != null) currentMap.under.MoveRight();
        currentMap = currentMap.MoveRight();
    }

    protected virtual void MoveUnder() {
        if (currentMap.left != null) currentMap.left.MoveUnder();
        if (currentMap.right != null) currentMap.right.MoveUnder();
        currentMap = currentMap.MoveUnder();
    }

    protected virtual void MoveUpper() {
        if (currentMap.left != null) currentMap.left.MoveUpper();
        if (currentMap.right != null) currentMap.right.MoveUpper();
        currentMap = currentMap.MoveUpper();
    }

    public virtual EzMap CreateMap() {
        EzMap map = randomPool.Create<EzMap>();
        if (map != null) {
            map.OnCreate();
        }
        return map;
    }

}
