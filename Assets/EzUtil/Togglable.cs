﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Togglable : MonoBehaviour {

    public bool disabled {
        get { return !enabled; }
        set { enabled = !value; }
    }

    public bool nonInteractable {
        get {
            Selectable selectable = GetComponent<Selectable>();
            return (selectable != null) ? !selectable.interactable : true;
        }
        set {
            Selectable selectable = GetComponent<Selectable>();
            if (selectable != null) {
                selectable.interactable = !value;
            }
        }
    }

    public void SetDeactive(bool deactive) {
        gameObject.SetActive(!deactive);
    }

}
