﻿using UnityEngine;
using System.Collections;

public class EzMap : MonoBehaviour {

    public bool autoSize;
    public float width;
    public float height;

    public Rect rect {
        get {
            return new Rect(
                transform.localPosition.x - width / 2,
                transform.localPosition.y - height / 2,
                width,
                height);
        }
    }

    public Vector3 leftPosition {
        get {
            return new Vector3(
                transform.localPosition.x - width / 2 - left.width / 2, 
                transform.localPosition.y, 
                transform.localPosition.z);
        }
    }

    public Vector3 rightPosition {
        get {
            return new Vector3(
                transform.localPosition.x + width / 2 + right.width / 2,
                transform.localPosition.y,
                transform.localPosition.z);
        }
    }

    public Vector3 underPosition {
        get {
            return new Vector3(
                transform.localPosition.x,
                transform.localPosition.y - height / 2 - under.height / 2,
                transform.localPosition.z);
        }
    }

    public Vector3 upperPosition {
        get {
            return new Vector3(
                transform.localPosition.x,
                transform.localPosition.y + height / 2 + upper.height / 2,
                transform.localPosition.z);
        }
    }

    public EzMap left { get; set; }
    public EzMap right { get; set; }
    public EzMap under { get; set; }
    public EzMap upper { get; set; }

    public EzMapSpawner mapSpawner {
        get {
            if (_mapSpawner == null) {
                _mapSpawner = GetComponentInParent<EzMapSpawner>();
            }
            return _mapSpawner;
        }
    }
    private EzMapSpawner _mapSpawner;

    protected Vector3 cameraPosition { get { return transform.parent.InverseTransformPoint(Camera.main.transform.position); } }

    void Awake() {
        if (autoSize) {
            SpriteRenderer sr = GetComponent<SpriteRenderer>();
            if (sr != null) {
                width = sr.bounds.size.x - 0.01f;
                height = sr.bounds.size.y - 0.01f;
            }
        }
    }

    public void Unlink() {
        if (left != null) left.right = right;
        if (right != null) right.left = left;
        if (under != null) under.upper = upper;
        if (upper != null) upper.under = under;
        left = right = under = upper = null;
    }

    public virtual void OnCreate() {

    }

    protected virtual void OnLink() {

    }

    public virtual void Recycle() {
        Unlink();
        mapSpawner.Recycle(transform);
    }

    public EzMap MoveLeft() {
        if (cameraPosition.x < rect.xMin) {
            if (left != null) {
                if (right != null) {
                    right.Recycle();
                }
                return left;
            }
        } else if (left == null) {
            left = mapSpawner.CreateMap();
            left.right = this;
            left.transform.localPosition = leftPosition;
            if (under != null) {
                left.under = under.left;
                if (under.left != null) {
                    under.left.upper = left;
                }
            }
            if (upper != null) {
                left.upper = upper.left;
                if (upper.left != null) {
                    upper.left.under = left;
                }
            }
            left.OnLink();
        }
        return this;
    }

    public EzMap MoveRight() {
        if (cameraPosition.x > rect.xMax) {
            if (right != null) {
                if (left != null) {
                    left.Recycle();
                }
                return right;
            }
        } else if (right == null) {
            right = mapSpawner.CreateMap();
            right.left = this;
            right.transform.localPosition = rightPosition;
            if (under != null) {
                right.under = under.right;
                if (under.right != null) {
                    under.right.upper = right;
                }
            }
            if (upper != null) {
                right.upper = upper.right;
                if (upper.right != null) {
                    upper.right.under = right;
                }
            }
            right.OnLink();
        }
        return this;
    }

    public EzMap MoveUnder() {
        if (cameraPosition.y < rect.yMin) {
            if (under != null) {
                if (upper != null) {
                    upper.Recycle();
                }
                return under;
            }
        } else if (under == null) {
            under = mapSpawner.CreateMap();
            under.upper = this;
            under.transform.localPosition = underPosition;
            if (left != null) {
                under.left = left.under;
                if (left.under != null) {
                    left.under.right = under;
                }
            }
            if (right != null) {
                under.right = right.under;
                if (right.under != null) {
                    right.under.left = under;
                }
            }
            under.OnLink();
        }
        return this;
    }

    public EzMap MoveUpper() {
        if (cameraPosition.y > rect.yMax) {
            if (upper != null) {
                if (under != null) {
                    under.Recycle();
                }
                return upper;
            }
        } else if (upper == null) {
            upper = mapSpawner.CreateMap();
            upper.under = this;
            upper.transform.localPosition = upperPosition;
            if (left != null) {
                upper.left = left.upper;
                if (left.upper != null) {
                    left.upper.right = upper;
                }
            }
            if (right != null) {
                upper.right = right.upper;
                if (right.upper != null) {
                    right.upper.left = upper;
                }
            }
            upper.OnLink();
        }
        return this;
    }
}
