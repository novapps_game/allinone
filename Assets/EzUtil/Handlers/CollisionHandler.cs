﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class CollisionEnterEvent : UnityEvent<Collision> { }

[System.Serializable]
public class CollisionStayEvent : UnityEvent<Collision> { }

[System.Serializable]
public class CollisionExitEvent : UnityEvent<Collision> { }

public class CollisionHandler : MonoBehaviour {

    public CollisionEnterEvent onCollisionEnter;
    [SerializeField]
    public LayerMask enterLayerMask;

    public CollisionStayEvent onCollisionStay;
    [SerializeField]
    public LayerMask stayLayerMask;

    public CollisionExitEvent onCollisionExit;
    [SerializeField]
    public LayerMask exitLayerMask;

    void OnCollisionEnter(Collision collision) {
        if (onCollisionEnter != null && ((1 << collision.gameObject.layer) & enterLayerMask.value) != 0) {
            onCollisionEnter.Invoke(collision);
        }
    }

    void OnCollisionStay(Collision collision) {
        if (onCollisionStay != null && ((1 << collision.gameObject.layer) & stayLayerMask.value) != 0) {
            onCollisionStay.Invoke(collision);
        }
    }

    void OnCollisionExit(Collision collision) {
        if (onCollisionExit != null && ((1 << collision.gameObject.layer) & exitLayerMask.value) != 0) {
            onCollisionExit.Invoke(collision);
        }
    }
}
