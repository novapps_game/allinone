﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DelayHandler : MonoBehaviour {

    [System.Serializable]
    public class DelayEvent : UnityEvent { }

    public DelayEvent onDelay;

    public void InvokeDelay(float delay) {
        Invoke("OnDelay", delay);
    }

    void OnDelay() {
        if (onDelay != null) {
            onDelay.Invoke();
        }
    }
}
