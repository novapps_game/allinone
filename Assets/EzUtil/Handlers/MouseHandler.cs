﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MouseHandler : MonoBehaviour {

    [System.Serializable]
    public class MouseDownEvent : UnityEvent { }
    public MouseDownEvent onMouseDown;

    [System.Serializable]
    public class MouseDragEvent : UnityEvent { }
    public MouseDragEvent onMouseDrag;

    [System.Serializable]
    public class MouseUpEvent : UnityEvent { }
    public MouseUpEvent onMouseUp;

    public void OnMouseDown() {
        if (onMouseDown != null) {
            onMouseDown.Invoke();
        }
    }

    public void OnMouseDrag() {
        if (onMouseDrag != null) {
            onMouseDrag.Invoke();
        }
    }

    public void OnMouseUp() {
        if (onMouseUp != null) {
            onMouseUp.Invoke();
        }
    }
}
