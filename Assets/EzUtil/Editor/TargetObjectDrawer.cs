﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;

#if !UNITY_3_5
[CustomPropertyDrawer(typeof(TargetObject))]
public class TargetObjectDrawer : PropertyDrawer {
#else
public class TargetObjectDrawer {
#endif

    class Entry {
        public Component root;
        public string name;
        public System.Type type;
        public Entry(Component root, string name, System.Type type) {
            this.root = root;
            this.name = name;
            this.type = type;
        }
    }

    static void GetObjects(Component root, string parentName, System.Type type, List<Entry> objects, HashSet<System.Type> filter) {
        BindingFlags flags = BindingFlags.Instance | BindingFlags.Public;
        FieldInfo[] fields = type.GetFields(flags);
        PropertyInfo[] props = type.GetProperties(flags);
        for (int i = 0; i < fields.Length; ++i) {
            FieldInfo field = fields[i];
            if (field.FieldType.IsSubclassOf(typeof(Object)) && !filter.Contains(field.FieldType)) {
                filter.Add(field.FieldType);
                string name = parentName + "/" + field.Name;
                objects.Add(new Entry(root, name, field.FieldType));
                GetObjects(root, name, field.FieldType, objects, filter);
                filter.Remove(field.FieldType);
            }
        }
        for (int j = 0; j < props.Length; ++j) {
            PropertyInfo prop = props[j];
            if (prop.CanRead && prop.PropertyType.IsSubclassOf(typeof(Object)) && !filter.Contains(prop.PropertyType)) {
                filter.Add(prop.PropertyType);
                string name = parentName + "/" + prop.Name;
                objects.Add(new Entry(root, name, prop.PropertyType));
                GetObjects(root, name, prop.PropertyType, objects, filter);
                filter.Remove(prop.PropertyType);
            }
        }
    }

    static List<Entry> GetObjects(Component[] components) {
        Dictionary<System.Type, int> dup = new Dictionary<System.Type, int>();
        List<Entry> objects = new List<Entry>();
        HashSet<System.Type> filter = new HashSet<System.Type>();
        filter.Add(typeof(GameObject));
        for (int i = 0; i < components.Length; ++i) {
            Component root = components[i];
            System.Type type = root.GetType();
            int count = 0;
            if (dup.ContainsKey(type)) {
                count = dup[type] + 1;
                dup[type] = count;
            } else {
                dup.Add(type, 0);
            }
            string name = count > 0 ? type.ToString() + "[" + count + "]" : type.ToString();
            Entry entry = new Entry(root, name, type);
            objects.Add(entry);
            filter.Add(type);
            GetObjects(root, name, type, objects, filter);
        }
        return objects;
    }

    static string[] GetNames(List<Entry> objects, string current, out int index) {
        index = -1;
        string[] names = new string[objects.Count];
        for (int i = 0; i < objects.Count; ++i) {
            names[i] = objects[i].name;
            if (index < 0 && current == names[i]) {
                index = i;
            }
        }
        return names;
    }

    /// <summary>
    /// Draw the actual property.
    /// </summary>
    public override void OnGUI(Rect rect, SerializedProperty prop, GUIContent label) {
        Component targetObject = prop.serializedObject.targetObject as Component;
        if (targetObject != null) {
            SerializedProperty root = prop.FindPropertyRelative("root");
            SerializedProperty path = prop.FindPropertyRelative("path");
            SerializedProperty typeName = prop.FindPropertyRelative("typeName");
            if (root.objectReferenceValue == null) {
                root.objectReferenceValue = targetObject.transform;
                path.stringValue = root.objectReferenceValue.GetType().ToString();
                typeName.stringValue = root.objectReferenceValue.GetType().FullName;
            }

            GUI.changed = false;
            EditorGUI.BeginDisabledGroup(root.hasMultipleDifferentValues);
            int index = 0;

            List<Entry> objects = GetObjects(targetObject.GetComponents<Component>());
            string[] names = GetNames(objects, path.stringValue, out index);

            EditorGUI.LabelField(rect, label);

            // Draw a selection list
            GUI.changed = false;
            rect.xMin += EditorGUIUtility.labelWidth;
            rect.width -= 18f;
            int choice = EditorGUI.Popup(rect, "", index, names);

            // Update the target object and property name
            if (GUI.changed && choice >= 0) {
                Entry entry = objects[choice];
                root.objectReferenceValue = entry.root;
                path.stringValue = entry.name;
                typeName.stringValue = entry.type.FullName;
            }
            EditorGUI.EndDisabledGroup();
        }
    }
}
