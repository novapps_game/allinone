﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EzTimer : MonoBehaviour {

    public float interval = 5f;
    public int times = 1;
    public bool runOnStart = true;

    [System.Serializable]
    public class TimerEvent : UnityEvent { }

    public TimerEvent onTimer;

    private CoroutineHandle handle;

    // Use this for initialization
    void Start() {
        if (runOnStart) {
            Run();
        }
    }

    public void Run() {
        if (times > 0) {
            handle = EzTiming.CallAtTimes(times, interval, OnTimer);
        } else {
            handle = EzTiming.CallForever(interval, OnTimer);
        }
    }

    void OnTimer() {
        if (onTimer != null) {
            onTimer.Invoke();
        }
    }

    private void OnDestroy() {
        if (handle != null) {
            EzTiming.KillCoroutines(handle);
        }
    }
}
