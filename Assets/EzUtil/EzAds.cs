﻿using System;
using UnityEngine;
using GoogleMobileAds.Api;
using System.Collections;

public class EzAds {

    public const bool debugInEditor = true;

    public static bool autoReloadBannerOnFailed = false;

    public static bool autoReloadInterstitialOnFailed = false;
    public static bool autoReloadInterstitialOnClosed = true;

    public static bool autoReloadRewardedVideoOnFailed = true;
    public static bool autoReloadRewardedVideoOnClosed = true;

    public static float autoReloadDelay = 2;

    public static int rewardAmount = 0;

    private static BannerView banner;
    private static InterstitialAd interstitial;
    private static RewardBasedVideoAd rewardedVideo;
    private static Action<int> rewardedAction;
    private static Action canceledAction;
    private static Action closeAction;

    private static bool bannerLoaded = false;
    private static bool requestingInterstitial = false;
    private static bool requestingRewardedVideo = false;

    private static bool rewarded = false;
    private static int rewardCount = 0;

    public static void RequestAll() {
        RequestBanner();
        RequestInterstitial();
        RequestRewardedVideo();
    }

    public static void RequestBanner() {
        string adUnitId = EzRemoteConfig.adBannerId;
        if (string.IsNullOrEmpty(adUnitId)) {
            Debug.LogWarning("banner unit id is null");
            return;
        }
        if (banner != null) banner.Destroy();
        // Create a 320x50 banner at the bottom of the screen.
        banner = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
        // Called when an ad request has successfully loaded.
        banner.OnAdLoaded += HandleOnBannerAdLoaded;
        // Called when an ad request failed to load.
        banner.OnAdFailedToLoad += HandleOnBannerAdFailedToLoad;
        // Called when an ad is clicked.
        banner.OnAdOpening += HandleOnBannerAdOpening;
        // Called when the user returned from the app after an ad click.
        banner.OnAdClosed += HandleOnBannerAdClosed;
        // Called when the ad click caused the user to leave the application.
        banner.OnAdLeavingApplication += HandleOnBannerAdLeavingApplication;
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the banner with the request.
        banner.LoadAd(request);
        Debug.Log("Start loading banner");
        EzAnalytics.LogEvent("Banner", "Load", "Start");
    }

    public static void ShowBanner() {
        if (banner != null && bannerLoaded) {
            banner.Show();
            EzAnalytics.LogEvent("Banner", "Show");
        }
    }

    public static void HideBanner() {
        if (banner != null) {
            banner.Hide();
            EzAnalytics.LogEvent("Banner", "Hide");
        }
    }

    public static void HandleOnBannerAdLoaded(object sender, EventArgs args) {
        Debug.Log("Banner loaded.");
        EzAnalytics.LogEvent("Banner", "Load", "Succeed");
        bannerLoaded = true;
    }

    public static void HandleOnBannerAdFailedToLoad(object sender, AdFailedToLoadEventArgs args) {
        Debug.Log("Banner failed to load: " + args.Message);
        EzAnalytics.LogEvent("Banner", "Load", "Failed");
        if (autoReloadBannerOnFailed) RequestBanner();
    }

    public static void HandleOnBannerAdOpening(object sender, EventArgs args) {
        Debug.Log("Banner opening.");
        EzAnalytics.LogEvent("Banner", "Open");
    }

    public static void HandleOnBannerAdClosed(object sender, EventArgs args) {
        Debug.Log("Banner closed.");
        EzAnalytics.LogEvent("Banner", "Close");
    }

    public static void HandleOnBannerAdLeavingApplication(object sender, EventArgs args) {
        Debug.Log("User left after clicked banner.");
        EzAnalytics.LogEvent("Banner", "Click");
    }

    public static void RequestInterstitial() {
        string adUnitId = EzRemoteConfig.adInterstitialId;
        if (string.IsNullOrEmpty(adUnitId)) {
            Debug.LogWarning("interstitial unit id is null");
            return;
        }
        if (requestingInterstitial) {
            Debug.Log("Loading interstitial, please wait.");
            return;
        }
        requestingInterstitial = true;
        if (interstitial != null) interstitial.Destroy();
        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);
        // Called when an ad request has successfully loaded.
        interstitial.OnAdLoaded += HandleOnInterstitialAdLoaded;
        // Called when an ad request failed to load.
        interstitial.OnAdFailedToLoad += HandleOnInterstitialAdFailedToLoad;
        // Called when an ad is clicked.
        interstitial.OnAdOpening += HandleOnInterstitialAdOpening;
        // Called when the user returned from the app after an ad click.
        interstitial.OnAdClosed += HandleOnInterstitialAdClosed;
        // Called when the ad click caused the user to leave the application.
        interstitial.OnAdLeavingApplication += HandleOnInterstitialAdLeavingApplication;
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
        Debug.Log("Start loading interstitial");
        EzAnalytics.LogEvent("Interstitial", "Load", "Start");
    }

    public static bool ShowInterstitial(Action onClose = null) {
        if (IsInterstitialLoaded()) {
            Debug.Log("Show interstitial");
            if (Application.platform == RuntimePlatform.WindowsEditor ||
                Application.platform == RuntimePlatform.OSXEditor) {
                if (onClose != null) {
                    onClose.Invoke();
                }
            } else {
                closeAction = onClose;
                interstitial.Show();
            }
            EzAnalytics.LogEvent("Interstitial", "Show");
            return true;
        }
        RequestInterstitial();
        return false;
    }

    public static bool IsInterstitialLoaded() {
        if (Application.platform == RuntimePlatform.WindowsEditor ||
            Application.platform == RuntimePlatform.OSXEditor) {
            return true;
        }
        return interstitial != null && interstitial.IsLoaded();
    }

    public static void HandleOnInterstitialAdLoaded(object sender, EventArgs args) {
        Debug.Log("Interstitial loaded.");
        EzAnalytics.LogEvent("Interstitial", "Load", "Succeed");
    }

    public static void HandleOnInterstitialAdFailedToLoad(object sender, AdFailedToLoadEventArgs args) {
        Debug.Log("Interstitial failed to load: " + args.Message);
        EzAnalytics.LogEvent("Interstitial", "Load", "Failed");
        requestingInterstitial = false;
        if (autoReloadInterstitialOnFailed) {
            EzTiming.CallDelayed(autoReloadDelay, RequestInterstitial);
        }
    }

    public static void HandleOnInterstitialAdOpening(object sender, EventArgs args) {
        Debug.Log("Interstitial opening.");
        EzAnalytics.LogEvent("Interstitial", "Open");
        requestingInterstitial = false;
    }

    public static void HandleOnInterstitialAdClosed(object sender, EventArgs args) {
        Debug.Log("Interstitial closed.");
        EzAnalytics.LogEvent("Interstitial", "Close");
        if (closeAction != null) {
            closeAction.Invoke();
        }
        if (autoReloadInterstitialOnClosed) {
            EzTiming.CallDelayed(autoReloadDelay, RequestInterstitial);
        }
    }

    public static void HandleOnInterstitialAdLeavingApplication(object sender, EventArgs args) {
        Debug.Log("User left after clicked interstitial.");
        EzAnalytics.LogEvent("Interstitial", "Click");
    }

    public static void RequestRewardedVideo() {
        string adUnitId = EzRemoteConfig.adRewardedVideoId;
        if (string.IsNullOrEmpty(adUnitId)) {
            Debug.LogWarning("rewarded video unit id is null");
            return;
        }
        if (requestingRewardedVideo) {
            Debug.Log("Loading rewarded video, please wait.");
            return;
        }
        requestingRewardedVideo = true;
        if (rewardedVideo == null) {
            // Initialize an RewardedVideoAd.
            rewardedVideo = RewardBasedVideoAd.Instance;
            // Called when an ad request has successfully loaded.
            rewardedVideo.OnAdLoaded += HandleOnRewardedVideoAdLoaded;
            // Called when an ad request failed to load.
            rewardedVideo.OnAdFailedToLoad += HandleOnRewardedVideoAdFailedToLoad;
            // Called when an ad is clicked.
            rewardedVideo.OnAdOpening += HandleOnRewardedVideoAdOpening;
            // has started playing.
            rewardedVideo.OnAdStarted += HandleOnRewardedVideoAdStarted;
            // has rewarded the user.
            rewardedVideo.OnAdRewarded += HandleOnRewardedVideoAdRewarded;
            // Called when the user returned from the app after an ad click.
            rewardedVideo.OnAdClosed += HandleOnRewardedVideoAdClosed;
            // Called when the ad click caused the user to leave the application.
            rewardedVideo.OnAdLeavingApplication += HandleOnRewardedVideoAdLeavingApplication;
            // Create an empty ad request.
        }
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewardVideo with the request.
        rewardedVideo.LoadAd(request, adUnitId);
        Debug.Log("Start loading rewarded video");
        EzAnalytics.LogEvent("RewardedVideo", "Load", "Start");
    }

    public static bool ShowRewardedVideo(Action<int> onRewarded, Action onCanceled = null) {
        if (IsRewardedVideoLoaded()) {
            Debug.Log("Show rewarded video");
            if (Application.platform == RuntimePlatform.WindowsEditor ||
                Application.platform == RuntimePlatform.OSXEditor) {
                if (onRewarded != null) {
                    onRewarded.Invoke(rewardAmount);
                }
            } else {
                rewardedAction = onRewarded;
                canceledAction = onCanceled;
                rewardedVideo.Show();
            }
            EzAnalytics.LogEvent("RewardedVideo", "Show");
            return true;
        }
        RequestRewardedVideo();
        return false;
    }

    public static bool IsRewardedVideoLoaded() {
#if UNITY_EDITOR
        return debugInEditor;
#else
        return rewardedVideo != null && rewardedVideo.IsLoaded();
#endif
    }

    public static void HandleOnRewardedVideoAdLoaded(object sender, EventArgs args) {
        Debug.Log("Rewarded video loaded. " + rewardedVideo.IsLoaded());
        EzAnalytics.LogEvent("RewardedVideo", "Load", "Succeed");
    }

    public static void HandleOnRewardedVideoAdFailedToLoad(object sender, AdFailedToLoadEventArgs args) {
        Debug.Log("Rewarded video failed to load: " + args.Message);
        EzAnalytics.LogEvent("RewardedVideo", "Load", "Failed");
        requestingRewardedVideo = false;
        if (autoReloadRewardedVideoOnFailed) {
            EzTiming.CallDelayed(autoReloadDelay, RequestRewardedVideo);
        }
    }

    public static void HandleOnRewardedVideoAdOpening(object sender, EventArgs args) {
        Debug.Log("Rewarded video opening.");
        EzAnalytics.LogEvent("RewardedVideo", "Open");
        requestingRewardedVideo = false;
    }

    public static void HandleOnRewardedVideoAdStarted(object sender, EventArgs args) {
        Debug.Log("Rewarded video started.");
        EzAnalytics.LogEvent("RewardedVideo", "Start");
    }

    public static void HandleOnRewardedVideoAdRewarded(object sender, Reward args) {
        Debug.Log("Rewarded video rewarded: type=" + args.Type + ", amount=" + args.Amount);
        EzAnalytics.LogEvent("RewardedVideo", "Reward");
        rewarded = true;
        rewardCount = rewardAmount > 0 ? rewardAmount : (int)args.Amount;
    }

    public static void HandleOnRewardedVideoAdClosed(object sender, EventArgs args) {
        Debug.Log("Rewarded video closed.");
        EzAnalytics.LogEvent("RewardedVideo", "Close");
        if (rewarded) {
            rewarded = false;
            if (rewardedAction != null) {
                rewardedAction.Invoke(rewardCount);
            }
        } else {
            if (canceledAction != null) {
                canceledAction.Invoke();
            }
        }
        if (autoReloadRewardedVideoOnClosed) {
            EzTiming.CallDelayed(autoReloadDelay, RequestRewardedVideo);
        }
    }

    public static void HandleOnRewardedVideoAdLeavingApplication(object sender, EventArgs args) {
        Debug.Log("User left after clicked rewarded video.");
        EzAnalytics.LogEvent("RewardedVideo", "Click");
    }
}
