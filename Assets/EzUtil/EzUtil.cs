﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class EzUtil {

    public static float NormalizeAngle(float angle) {
        while (angle > 180) angle -= 360;
        while (angle <= -180) angle += 360;
        return angle;
    }

    public static int[] RandomPerm(int min, int max) {
        int[] perm = new int[max - min];
        for (int i = min; i < max; ++i) {
            perm[i - min] = i;
        }
        System.Array.Sort(perm, new RandomComparer<int>());
        return perm;
    }

    public static T RandomPick<T>(IList<T> list) {
        if (list.Count <= 0) return default(T);
        int index = Random.Range(0, list.Count);
        T value = list[index];
        list.RemoveAt(index);
        return value;
    }

    public static System.Type GetType(string TypeName) {

        // Try Type.GetType() first. This will work with types defined
        // by the Mono runtime, in the same assembly as the caller, etc.
        var type = System.Type.GetType(TypeName);

        // If it worked, then we're done here
        if (type != null)
            return type;

        // If the TypeName is a full name, then we can try loading the defining assembly directly
        if (TypeName.Contains(".")) {

            // Get the name of the assembly (Assumption is that we are using 
            // fully-qualified type names)
            var assemblyName = TypeName.Substring(0, TypeName.IndexOf('.'));

            // Attempt to load the indicated Assembly
            var assembly = System.Reflection.Assembly.Load(assemblyName);
            if (assembly == null)
                return null;

            // Ask that assembly to return the proper Type
            type = assembly.GetType(TypeName);
            if (type != null)
                return type;

        }

        // If we still haven't found the proper type, we can enumerate all of the 
        // loaded assemblies and see if any of them define the type
        var currentAssembly = System.Reflection.Assembly.GetExecutingAssembly();
        var referencedAssemblies = currentAssembly.GetReferencedAssemblies();
        foreach (var assemblyName in referencedAssemblies) {

            // Load the referenced assembly
            var assembly = System.Reflection.Assembly.Load(assemblyName);
            if (assembly != null) {
                // See if that assembly defines the named type
                type = assembly.GetType(TypeName);
                if (type != null)
                    return type;
            }
        }

        // The type just couldn't be found...
        return null;
    }
}
