﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WebSprite : MonoBehaviour {

    [System.Serializable]
    public class LoadEvent : UnityEvent<Sprite> { }

    public string url;
    public bool loadOnAwake = true;
    public Sprite placeholder;
    public LoadEvent onLoaded;

    private SpriteRenderer spriteRenderer;

    void Awake() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (spriteRenderer == null) {
            spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
        }
        if (placeholder != null) {
            UpdateSprite(placeholder);
        }
        if (loadOnAwake) {
            Load();
        }
    }

    public void Load() {
        if (string.IsNullOrEmpty(url)) return;
        StartCoroutine(EzImageLoader.LoadSprite(url, (sprite) => {
            UpdateSprite(sprite);
            if (onLoaded != null) {
                onLoaded.Invoke(sprite);
            }
        }));
    }

    public void UpdateSprite(Sprite sprite) {
        spriteRenderer.sprite = sprite;
    }
}
