﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EzRandom : MonoBehaviour {

    public static bool YesOrNo() {
        return Random.Range(0, 2) > 0;
    }

    public static T Select<T>(T[] array) {
        return array[Random.Range(0, array.Length)];
    }

    public static T Select<T>(IList<T> array) {
        return array[Random.Range(0, array.Count)];
    }

    public static T Pick<T>(IList<T> list) {
        if (list.Count <= 0) return default(T);
        int index = Random.Range(0, list.Count);
        T value = list[index];
        list.RemoveAt(index);
        return value;
    }

    // return random permutation of [from, to)
    public static int[] Perm(int from, int to) {
        int[] perm = new int[to - from];
        for (int i = 0, j = from; j < to; ++i, ++j) {
            perm[i] = j;
        }
        System.Array.Sort(perm, new RandomComparer<int>());
        return perm;
    }

    public static int Roll(float[] chances, bool takeAway = false) {
        Vector2[] sections = new Vector2[chances.Length];
        float min = 0f, max = 0f;
        for (int i = 0; i < chances.Length; ++i) {
            max += chances[i];
            sections[i] = new Vector2(min, max);
            min = max;
        }
        float r = Random.Range(0, max - Mathf.Epsilon);
        for (int j = 0; j < sections.Length; ++j) {
            if (r >= sections[j].x && r < sections[j].y) {
                if (takeAway) {
                    chances[j] = 0;
                }
                return j;
            }
        }
        return -1;
    }

    public static Vector2 Position(Vector2 min, Vector2 max) {
        return new Vector2(
            Random.Range(min.x, max.x), 
            Random.Range(min.y, max.y));
    }

    public static Vector3 Position(Vector3 min, Vector3 max) {
        return new Vector3(
            Random.Range(min.x, max.x),
            Random.Range(min.y, max.y),
            Random.Range(min.z, max.z));
    }

    public static Vector2 Position(Vector2 min, Vector2 max, Vector2Int dimension) {
        Vector2 size = max - min;
        Vector2 cellSize = new Vector2(size.x / dimension.x, size.y / dimension.y);
        Vector2Int location = new Vector2Int(
            Random.Range(0, dimension.x), 
            Random.Range(0, dimension.y));
        return new Vector2(
            min.x + cellSize.x * location.x,
            min.y + cellSize.y * location.y);
    }

    public static Vector3 Position(Vector3 min, Vector3 max, Vector3Int dimension) {
        Vector3 size = max - min;
        Vector3 cellSize = new Vector3(size.x / dimension.x, size.y / dimension.y, size.z / dimension.z);
        Vector3Int location = new Vector3Int(
            Random.Range(0, dimension.x), 
            Random.Range(0, dimension.y), 
            Random.Range(0, dimension.z));
        return new Vector3(
            min.x + cellSize.x * location.x,
            min.y + cellSize.y * location.y,
            min.z + cellSize.z * location.z);
    }
}
