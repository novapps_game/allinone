﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EzRemoteConfig {

    private const string ONLINE = "online";
    private const string NO_ADS = "no_ads";

    private const string AD_BANNER_ID = "ad_banner_id";
    private const string AD_INTERSTITIAL_ID = "ad_interstitial_id";
    private const string AD_REWARDEDVIDEO_ID = "ad_rewardedvideo_id";

    private const string AD_SHOW_FIRST = "ad_show_first";
    private const string AD_SHOW_INTERVAL = "ad_show_interval";
    private const string AD_SHOW_TIME_INTERVAL = "ad_show_time_interval";

    private const string AD_REWARD_AMOUNT = "ad_reward_amount";

    private const string RATE_URL = "rate_url";
    private const string RATE_TITLE = "rate_title";
    private const string RATE_BUTTON = "rate_button";
    private const string RATE_FIRST = "rate_first";
    private const string RATE_INTERVAL = "rate_interval";
    private const string RATE_MAX_COUNT = "rate_max_count";
    private const string RATE_MIN_TIME = "rate_min_time";
    private const string RATE_FORCE = "rate_force";

    private const string PROMO_IDS = "promo_ids";
    private const string PROMO_URLS = "promo_urls";
    private const string PROMO_IMAGES = "promo_images";
    private const string PROMO_INTERVAL = "promo_interval";
    private const string PROMO_MAX_COUNT = "promo_max_count";
    private const string PROMO_FORCE = "promo_force";

    private const string UPGRADE_URL = "upgrade_url";
    private const string UPGRADE_TITLE = "upgrade_title";
    private const string UPGRADE_BODY = "upgrade_body";
    private const string UPGRADE_FORCE = "upgrade_force";

    private const string SHARE_APP_URL = "share_app_url";
    private const string SHARE_APP_TEXT = "share_app_text";
    private const string SHARE_APP_IMAGE = "share_app_image";

    private const string SHARE_SCORE_URL = "share_score_url";
    private const string SHARE_SCORE_TEXT = "share_score_text";
    private const string SHARE_SCORE_IMAGE = "share_score_image";
    private const string SHARE_SCORE_OFFSET_X = "share_score_offset_x";
    private const string SHARE_SCORE_OFFSET_Y = "share_score_offset_y";

    private const string SHARE_REWARD_TITLE = "share_reward_title";
    private const string SHARE_REWARD_BODY = "share_reward_body";
    private const string SHARE_REWARD_BUTTON = "share_reward_button";
    private const string SHARE_REWARD_URL = "share_reward_url";
    private const string SHARE_REWARD_TEXT = "share_reward_text";

    private const string SHARE_VIRUS_TITLE = "share_virus_title";
    private const string SHARE_VIRUS_BODY = "share_virus_body";
    private const string SHARE_VIRUS_REMARK = "share_virus_remark";
    private const string SHARE_VIRUS_BUTTON = "share_virus_button";
    private const string SHARE_VIRUS_URL = "share_virus_url";
    private const string SHARE_VIRUS_TEXT = "share_virus_text";

    private const string SHARE_VIDEO_TITLE = "share_video_title";

    private const string REVIVE_COST = "revive_cost";
    private const string REVIVE_MIN_TIME = "revive_min_time";
    private const string REVIVE_MAX_COUNT = "revive_max_count";

    private const string COIN_NAME = "coin_name";
    private const string COINS_NAME = "coins_name";

    private const string DIAMOND_NAME = "diamond_name";
    private const string DIAMONDS_NAME = "diamonds_name";

    public static bool online { get { return GetBool(ONLINE); } }
    public static bool noAds { get { return GetBool(NO_ADS); } }

    public static string adBannerId { get { return GetString(AD_BANNER_ID); } }
    public static string adInterstitialId { get { return GetString(AD_INTERSTITIAL_ID); } }
    public static string adRewardedVideoId { get { return GetString(AD_REWARDEDVIDEO_ID); } }

    public static int adShowFirst { get { return GetInt(AD_SHOW_FIRST); } }
    public static int adShowInterval { get { return GetInt(AD_SHOW_INTERVAL); } }
    public static float adShowTimeInterval { get { return GetFloat(AD_SHOW_TIME_INTERVAL); } }

    public static int adRewardAmount { get { return GetInt(AD_REWARD_AMOUNT); } }

    public static string rateUrl { get { return GetString(RATE_URL); } }
    public static string rateTitle { get { return GetString(RATE_TITLE); } }
    public static string rateButton { get { return GetString(RATE_BUTTON); } }
    public static int rateFirst { get { return GetInt(RATE_FIRST); } }
    public static int rateInterval { get { return GetInt(RATE_INTERVAL); } }
    public static int rateMaxCount { get { return GetInt(RATE_MAX_COUNT); } }
    public static float rateMinTime { get { return GetFloat(RATE_MIN_TIME); } }
    public static bool rateForce { get { return GetBool(RATE_FORCE); } }

    public static string[] promoIds { get { return GetString(PROMO_IDS).Split(','); } }
    public static string[] promoUrls { get { return GetString(PROMO_URLS).Split(','); } }
    public static string[] promoImages { get { return GetString(PROMO_IMAGES).Split(','); } }
    public static int promoInterval { get { return GetInt(PROMO_INTERVAL); } }
    public static int promoMaxCount { get { return GetInt(PROMO_MAX_COUNT); } }
    public static bool promoForce { get { return GetBool(PROMO_FORCE); } }

    public static string upgradeUrl { get { return GetString(UPGRADE_URL); } }
    public static string upgradeTitle { get { return GetString(UPGRADE_TITLE); } }
    public static string upgradeBody { get { return GetString(UPGRADE_BODY); } }
    public static bool upgradeForce { get { return GetBool(UPGRADE_FORCE); } }

    public static string shareAppUrl { get { return GetString(SHARE_APP_URL); } }
    public static string shareAppText { get { return GetString(SHARE_APP_TEXT); } }
    public static string shareAppImage { get { return GetString(SHARE_APP_IMAGE); } }

    public static string shareScoreUrl { get { return GetString(SHARE_SCORE_URL); } }
    public static string shareScoreText { get { return GetString(SHARE_SCORE_TEXT); } }
    public static string shareScoreImage { get { return GetString(SHARE_SCORE_IMAGE); } }
    public static int shareScoreOffsetX { get { return GetInt(SHARE_SCORE_OFFSET_X); } }
    public static int shareScoreOffsetY { get { return GetInt(SHARE_SCORE_OFFSET_Y); } }

    public static string shareRewardTitle { get { return GetString(SHARE_REWARD_TITLE); } }
    public static string shareRewardBody { get { return GetString(SHARE_REWARD_BODY); } }
    public static string shareRewardButton { get { return GetString(SHARE_REWARD_BUTTON); } }
    public static string shareRewardUrl { get { return GetString(SHARE_REWARD_URL); } }
    public static string shareRewardText { get { return GetString(SHARE_REWARD_TEXT); } }

    public static string shareVirusTitle { get { return GetString(SHARE_VIRUS_TITLE); } }
    public static string shareVirusBody { get { return GetString(SHARE_VIRUS_BODY); } }
    public static string shareVirusRemark { get { return GetString(SHARE_VIRUS_REMARK); } }
    public static string shareVirusButton { get { return GetString(SHARE_VIRUS_BUTTON); } }
    public static string shareVirusUrl { get { return GetString(SHARE_VIRUS_URL); } }
    public static string shareVirusText { get { return GetString(SHARE_VIRUS_TEXT); } }

    public static string shareVideoTitle { get { return GetString(SHARE_VIDEO_TITLE); } }

    public static int reviveCost { get { return GetInt(REVIVE_COST); } }
    public static float reviveMinTime { get { return GetFloat(REVIVE_MIN_TIME); } }
    public static int reviveMaxCount { get { return GetInt(REVIVE_MAX_COUNT); } }

    public static string coinName { get { return GetString(COIN_NAME); } }
    public static string coinsName { get { return GetString(COINS_NAME); } }

    public static string diamondName { get { return GetString(DIAMOND_NAME); } }
    public static string diamondsName { get { return GetString(DIAMONDS_NAME); } }

    private static string updateUrl {
        get {
            return "http://service.kv.dandanjiang.tv/remote?appid=" + EzIds.appID +
            "&appver=" + Application.version + "&os=" + EzDevice.platform + "&lan=" + Localization.GetLanguage();
        }
    }
    private static readonly float defaultTimeout = 10;
    private static readonly float updateTimeout = 3;

    private static IDictionary<string, string> configs;

    public static IEnumerator Init(System.Action onUpdate) {
        Debug.Log("Initializing remote config");
        Dictionary<string, string> defaults = new Dictionary<string, string>();
        defaults.Add(ONLINE, "false");
        defaults.Add(NO_ADS, "false");

        defaults.Add(AD_BANNER_ID, "");
        defaults.Add(AD_INTERSTITIAL_ID, "");
        defaults.Add(AD_REWARDEDVIDEO_ID, "");

        defaults.Add(AD_SHOW_FIRST, "3");
        defaults.Add(AD_SHOW_INTERVAL, "3");
        defaults.Add(AD_SHOW_TIME_INTERVAL, "60");

        defaults.Add(AD_REWARD_AMOUNT, "30");

        defaults.Add(RATE_URL, "");
        defaults.Add(RATE_TITLE, Localization.GetMultilineText("RateTitle"));
        defaults.Add(RATE_BUTTON, Localization.GetMultilineText("RateUs"));
        defaults.Add(RATE_FIRST, "2");
        defaults.Add(RATE_INTERVAL, "3");
        defaults.Add(RATE_MAX_COUNT, "3");
        defaults.Add(RATE_MIN_TIME, "10");
		defaults.Add(RATE_FORCE, "false");
		
        defaults.Add(PROMO_IDS, "");
        defaults.Add(PROMO_URLS, "");
        defaults.Add(PROMO_IMAGES, "");
        defaults.Add(PROMO_INTERVAL, "2");
        defaults.Add(PROMO_MAX_COUNT, "1");
        defaults.Add(PROMO_FORCE, "false");

        defaults.Add(UPGRADE_URL, "");
        defaults.Add(UPGRADE_TITLE, Localization.GetMultilineText("UpgradeTitle"));
        defaults.Add(UPGRADE_BODY, Localization.GetMultilineText("UpgradeBody"));
        defaults.Add(UPGRADE_FORCE, "false");

        defaults.Add(SHARE_APP_URL, "");
        defaults.Add(SHARE_APP_TEXT, Localization.GetMultilineText("ShareAppText"));
        defaults.Add(SHARE_APP_IMAGE, "");

        defaults.Add(SHARE_SCORE_URL, "");
        defaults.Add(SHARE_SCORE_TEXT, Localization.GetMultilineText("ShareScoreText"));
        defaults.Add(SHARE_SCORE_IMAGE, "");
        defaults.Add(SHARE_SCORE_OFFSET_X, "0");
        defaults.Add(SHARE_SCORE_OFFSET_Y, "0");

        defaults.Add(SHARE_REWARD_TITLE, Localization.GetMultilineText("ShareRewardTitle"));
        defaults.Add(SHARE_REWARD_BODY, Localization.GetMultilineText("ShareRewardBody"));
        defaults.Add(SHARE_REWARD_BUTTON, Localization.GetMultilineText("Share"));
        defaults.Add(SHARE_REWARD_URL, "http://share.game.adesk.com/reward");
        defaults.Add(SHARE_REWARD_TEXT, Localization.GetMultilineText("ShareRewardText"));

        defaults.Add(SHARE_VIRUS_TITLE, Localization.GetMultilineText("ShareVirusTitle"));
        defaults.Add(SHARE_VIRUS_BODY, Localization.GetMultilineText("ShareVirusBody"));
        defaults.Add(SHARE_VIRUS_REMARK, Localization.GetMultilineText("ShareVirusRemark"));
        defaults.Add(SHARE_VIRUS_BUTTON, Localization.GetMultilineText("Share"));
        defaults.Add(SHARE_VIRUS_URL, "http://share.game.adesk.com/virus");
        defaults.Add(SHARE_VIRUS_TEXT, Localization.GetMultilineText("ShareVirusText"));

        defaults.Add(SHARE_VIDEO_TITLE, Localization.GetMultilineText("ShareVideoTitle"));

        defaults.Add(REVIVE_COST, "30");
        defaults.Add(REVIVE_MIN_TIME, "8");
        defaults.Add(REVIVE_MAX_COUNT, "0");

        defaults.Add(COIN_NAME, Localization.GetText("Coin"));
        defaults.Add(COINS_NAME, Localization.GetText("Coins"));

        defaults.Add(DIAMOND_NAME, Localization.GetText("Diamond"));
        defaults.Add(DIAMONDS_NAME, Localization.GetText("Diamonds"));

        yield return Init(defaults, onUpdate);
    }

    public static IEnumerator Init(IDictionary<string, string> defaults, System.Action onUpdate) {
        SetDefaults(defaults);
        yield return Update(updateTimeout, onUpdate);
    }

    public static void SetDefaults(IDictionary<string, string> defaults) {
        configs = defaults;
    }

    public static IEnumerator Update(float timeout, System.Action onUpdate = null) {
        yield return EzRestApi.Get(updateUrl, timeout, (json) => {
            Dictionary<string, object> data = MiniJSON.Json.Deserialize(json) as Dictionary<string, object>;
            long code = (long)data["code"];
            if (code == 0) {
                Dictionary<string, object> res = data["res"] as Dictionary<string, object>;
                foreach (var pair in res) {
                    Debug.Log(pair.Key + " = " + pair.Value);
                    configs[pair.Key] = pair.Value.ToString().Replace("\\n", "\n").Replace("\\t", "\t");
                }
            }
            if (onUpdate != null) {
                onUpdate.Invoke();
            }
        }, (error) => onUpdate(), () => Update(defaultTimeout));
    }

    public static bool GetBool(string key) {
        return bool.Parse(configs[key]);
    }

    public static int GetInt(string key) {
        return int.Parse(configs[key]);
    }

    public static long GetLong(string key) {
        return long.Parse(configs[key]);
    }

    public static float GetFloat(string key) {
        return float.Parse(configs[key]);
    }

    public static double GetDouble(string key) {
        return double.Parse(configs[key]);
    }

    public static string GetString(string key) {
        return configs[key];
    }
}
