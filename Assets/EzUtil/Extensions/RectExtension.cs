﻿using UnityEngine;
using System.Collections;

public static class RectExtension {

    public static Bounds ToBounds(this Rect self) {
        return new Bounds(self.center, self.size);
    }

    public static Rect NewX(this Rect self, float x) {
        return new Rect(x, self.y, self.width, self.height);
    }

    public static Rect NewY(this Rect self, float y) {
        return new Rect(self.x, y, self.width, self.height);
    }

    public static Rect NewXY(this Rect self, float x, float y) {
        return new Rect(x, y, self.width, self.height);
    }

    public static Rect NewW(this Rect self, float width) {
        return new Rect(self.x, self.y, width, self.height);
    }

    public static Rect NewH(this Rect self, float height) {
        return new Rect(self.x, self.y, self.width, height);
    }

    public static Rect NewWH(this Rect self, float width, float height) {
        return new Rect(self.x, self.y, width, height);
    }

    public static Rect NewPosition(this Rect self, Vector2 position) {
        return new Rect(position, self.size);
    }

    public static Rect NewSize(this Rect self, Vector2 size) {
        return new Rect(self.position, size);
    }

    public static Rect GetZoomed(this Rect self, float size) {
        return self.GetZoomed(size, size, size, size);
    }

    public static Rect GetZoomed(this Rect self, float left, float bottom) {
        return self.GetZoomed(left, bottom, left, bottom);
    }

    public static Rect GetZoomed(this Rect self, float left, float bottom, float right, float top) {
        return new Rect(self.x - left, self.y - bottom, self.width + left + right, self.height + bottom + top);
    }

    public static Rect GetScaled(this Rect self, float scale) {
        return self.GetScaled(scale, scale);
    }

    public static Rect GetScaled(this Rect self, float scaleX, float scaleY) {
        float width = self.width * scaleX;
        float height = self.height * scaleY;
        float x = self.center.x - width * 0.5f;
        float y = self.center.y - height * 0.5f;
        return new Rect(x, y, width, height);
    }

    public static Vector2 RandomPosition(this Rect self) {
        return new Vector2(Random.Range(self.xMin, self.xMax), Random.Range(self.yMin, self.yMax));
    }

    public static Vector2 RandomPositionOnEdge(this Rect self) {
        Vector2 position = self.RandomPosition();
        int edge = Random.Range(0, 4);
        switch (edge) {
            case 0: position.x = self.xMax; break;
            case 1: position.y = self.yMax; break;
            case 2: position.x = self.xMin; break;
            case 3: position.y = self.yMin; break;
        }
        return position;
    }
}
