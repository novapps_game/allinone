﻿using UnityEngine;
using System.Collections;

public static class ColorExtension {

	public static Color NewR(this Color self, float r) {
        return new Color(r, self.g, self.b, self.a);
    }

    public static Color NewG(this Color self, float g) {
        return new Color(self.r, g, self.b, self.a);
    }

    public static Color NewB(this Color self, float b) {
        return new Color(self.r, self.g, b, self.a);
    }

    public static Color NewA(this Color self, float a) {
        return new Color(self.r, self.g, self.b, a);
    }

    public static Color NewRGB(this Color self, Color rgb) {
        return new Color(rgb.r, rgb.g, rgb.b, self.a);
    }

    public static HSVColor ToHSV(this Color self) {
        return new HSVColor(self);
    }

    public static Color NewH(this Color self, int h) {
        HSVColor hsvColor = self.ToHSV();
        hsvColor.h = h;
        return hsvColor.rgb;
    }

    public static Color NewS(this Color self, int s) {
        HSVColor hsvColor = self.ToHSV();
        hsvColor.s = s;
        return hsvColor.rgb;
    }

    public static Color NewV(this Color self, int v) {
        HSVColor hsvColor = self.ToHSV();
        hsvColor.v = v;
        return hsvColor.rgb;
    }

    public static Color NewHSV(this Color self, HSVColor hsv) {
        return NewRGB(self, hsv.ToRGB());
    }

    public static Color Fade(this Color from, float a, float t) {
        return NewA(from, Mathf.Lerp(from.a, a, t));
    }

    public static Color Tint(this Color from, Color to, float t) {
        return new Color(
            Mathf.Lerp(from.r, to.r, t),
            Mathf.Lerp(from.g, to.g, t),
            Mathf.Lerp(from.b, to.b, t),
            Mathf.Lerp(from.a, to.a, t));
    }

    public static string ToHexString(this Color self, string prefix = "#") {
        Color32 color = self;
        return string.Format(prefix + "{0:X2}{1:X2}{2:X2}", color.r, color.g, color.b);
    }
}
