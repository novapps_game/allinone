﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BoundsExtension {

    public static bool Contains(this Bounds source, Bounds target) {
        return source.Contains(target.min) && source.Contains(target.max);
    }

    public static Bounds GetMerged(this Bounds source, Bounds target) {
        if (source.size == Vector3.zero) {
            return target;
        }
        Vector3 min = new Vector3();
        Vector3 max = new Vector3();
        min.x = Mathf.Min(source.min.x, target.min.x);
        min.y = Mathf.Min(source.min.y, target.min.y);
        min.z = Mathf.Min(source.min.z, target.min.z);
        max.x = Mathf.Max(source.max.x, target.max.x);
        max.y = Mathf.Max(source.max.y, target.max.y);
        max.z = Mathf.Max(source.max.z, target.max.z);
        source.min = min;
        source.max = max;
        return source;
    }

    public static Vector3 RandomPosition(this Bounds self) {
        return new Vector3(
            Random.Range(self.min.x, self.max.x),
            Random.Range(self.min.y, self.max.y),
            Random.Range(self.min.z, self.max.z));
    }

    public static Vector3 RandomPositionOnEdge(this Bounds self) {
        Vector3 position = self.RandomPosition();
        int edge = Random.Range(0, 6);
        switch (edge) {
            case 0: position.x = self.max.x; break;
            case 1: position.y = self.max.y; break;
            case 2: position.z = self.max.z; break;
            case 3: position.x = self.min.x; break;
            case 4: position.y = self.min.y; break;
            case 5: position.y = self.min.z; break;
        }
        return position;
    }
}
