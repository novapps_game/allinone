﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Digit {

    public string name;
    public string symbol;
    public string prefix;
    public int place;
    public double unit;

    public Digit(string name, int place)
        : this(name, name, place) {
    }

    public Digit(string name, string symbol, int place)
        : this(name, symbol, "", place) {
    }

    public Digit(string name, string symbol, string prefix, int place) {
        this.name = name;
        this.symbol = symbol;
        this.prefix = prefix;
        this.place = place;
        this.unit = System.Math.Pow(10, place);
    }

    public static string Format(double value) {
        return Format(value, DIGITS);
    }

    public static string Format(double value, Digit[] digits) {
        double v = 0;
        int i = 0;
        for (; i < digits.Length - 1; ++i) {
            if (System.Math.Abs(value) < digits[i + 1].unit) {
                return (value / digits[i].unit).ToString("G" + (digits[i + 1].place - digits[i].place)) + digits[i].symbol;
            }
        }
        v = value / digits[i].unit;
        return v.ToString("G") + digits[i].symbol;
    }

    public static Digit[] DIGITS {
        get {
            if (Localization.GetLanguage() == Localization.LANGUAGE_CHINESE) {
                return DIGITS_CN;
            } else if (Localization.GetLanguage() == Localization.LANGUAGE_CHINESE_TRADITIONAL) {
                return DIGITS_CT;
            }
            return DIGITS_EN;
        }
    }

    public static readonly Digit[] DIGITS_EN = {
        new Digit("", 0),
        new Digit("Thousand",       "k",    "Kilo",     3),
        new Digit("Million",        "M",    "Mega",     6),
        new Digit("Billion",        "G",    "Giga",     9),
        new Digit("Trillion",       "T",    "Tera",     12),
        new Digit("Quadrillion",    "P",    "Peta",     15),
        new Digit("Quintillion",    "E",    "Exa",      18),
        new Digit("Sextillion",     "Z",    "Zetta",    21),
        new Digit("Septillion",     "Y",    "Yotta",    24),
        new Digit("Octillion",      "X",    "Xenna",    27),
    };

    public static readonly Digit[] DIGITS_CN = {
        new Digit("", 0),
        new Digit("万",  4),
        new Digit("亿",  8),
        new Digit("万亿",12),
        new Digit("兆",  16),
        new Digit("万兆",20),
        new Digit("京",  24),
        new Digit("万京",28),
        new Digit("垓",  32),
        new Digit("万垓",36),
        new Digit("秭",  40),
        new Digit("万秭",44),
        new Digit("穰",  48),
        new Digit("万穰",52),
        new Digit("溝",  56),
        new Digit("万穰",60),
        new Digit("澗",  64),
        new Digit("万澗",68),
        new Digit("正",  72),
        new Digit("万正",76),
        new Digit("載",  80),
        new Digit("万載",84),
        new Digit("极",  88),
    };

    public static readonly Digit[] DIGITS_CT = {
        new Digit("", 0),
        new Digit("萬", 4),
        new Digit("億", 8),
        new Digit("萬億",12),
        new Digit("兆", 16),
        new Digit("萬兆",20),
        new Digit("京", 24),
        new Digit("萬京",28),
        new Digit("垓", 32),
        new Digit("萬垓",36),
        new Digit("秭", 40),
        new Digit("萬秭",44),
        new Digit("穰", 48),
        new Digit("萬穰",52),
        new Digit("溝", 56),
        new Digit("萬穰",60),
        new Digit("澗", 64),
        new Digit("萬澗",68),
        new Digit("正", 72),
        new Digit("萬正",76),
        new Digit("載", 80),
        new Digit("萬載",84),
        new Digit("極", 88),
    };
}
