﻿using System.IO;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine;

public class EzShareNative {

	private static readonly string SCREENSHOT = Application.persistentDataPath + Path.DirectorySeparatorChar + "screenshot.jpg";

	public static void ShareScreenshot(string text = "", string subject = "", string title = "") {
		EzScreenshot.Capture(SCREENSHOT, (path) => {
			ShareImage(path, text, subject, title);
		});
	}

	public static void ShareScreenshot(Rect fromRect, string text = "", string subject = "", string title = "") {
		EzScreenshot.Capture(SCREENSHOT, fromRect, (path) => {
			ShareImage(path, text, subject, title);
		});
	}

	public static void ShareScreenshot(RectTransform rectTransform, string text = "", string subject = "", string title = "") {
		EzScreenshot.Capture(SCREENSHOT, rectTransform, (path) => {
			ShareImage(path, text, subject, title);
		});
	}

	public static void ShareScreenshot(Rect fromRect, Texture2D toTex, int toX, int toY, 
		string text = "", string subject = "", string title = "") {
		EzScreenshot.Capture(SCREENSHOT, fromRect, toTex, toX, toY, (path) => {
			ShareImage(path, text, subject, title);
		});
	}

	public static void ShareScreenshot(RectTransform rectTransform, Texture2D toTex, int toX, int toY, 
		string text = "", string subject = "", string title = "") {
		EzScreenshot.Capture(SCREENSHOT, rectTransform, toTex, toX, toY, (path) => {
			ShareImage(path, text, subject, title);
		});
	}

	public static void ShareScreenshot(Rect fromRect, Texture2D toTex, Rect toRect, 
		string text = "", string subject = "", string title = "") {
		EzScreenshot.Capture(SCREENSHOT, fromRect, toTex, toRect, (path) => {
			ShareImage(path, text, subject, title);
		});
	}

	public static void ShareScreenshot(RectTransform rectTransform, Texture2D toTex, Rect toRect, 
		string text = "", string subject = "", string title = "") {
		EzScreenshot.Capture(SCREENSHOT, rectTransform, toTex, toRect, (path) => {
			ShareImage(path, text, subject, title);
		});
    }

	public static void ShareText(string text, string url, string subject = "", string title = "") {
		if (Application.platform == RuntimePlatform.Android) {
#if UNITY_ANDROID
            text = text + url;
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
			AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
			intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
			intentObject.Call<AndroidJavaObject>("setType", "text/plain");
			if (!string.IsNullOrEmpty(subject)) {
				intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), subject);
			}
			if (!string.IsNullOrEmpty(title)) {
				intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TITLE"), title);
			}
			intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), text);
			intentObject.Call<AndroidJavaObject>("putExtra", "Kdescription", text);
			AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, Localization.GetText("ShareTo"));
			currentActivity.Call("startActivity", jChooser);
#endif
		} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
#if UNITY_IOS
			shareText(text, url);
#endif
		} else {
			Debug.Log("No sharing set up for this platform.");
		}
	}

	public static void ShareImage(string imagePath, string text = "", string subject = "", string title = "") {
        if (Application.platform == RuntimePlatform.Android) {
#if UNITY_ANDROID
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            AndroidJavaObject fileObject = new AndroidJavaObject("java.io.File", imagePath);
			if (!fileObject.Call<bool>("exists")) {
				Debug.Log("Cannot find share image file: " + imagePath);
				return;
			}
			AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
            AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("fromFile", fileObject);
            intentObject.Call<AndroidJavaObject>("setType", "image/*");
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
			if (!string.IsNullOrEmpty(subject)) {
				intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), subject);
			}
			if (!string.IsNullOrEmpty(title)) {
				intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TITLE"), title);
			}
			if (!string.IsNullOrEmpty(text)) {
	            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), text);
			}
            intentObject.Call<AndroidJavaObject>("putExtra", "Kdescription", text);

            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

			AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, Localization.GetText("ShareTo"));
            currentActivity.Call("startActivity", jChooser);
#endif
        } else if (Application.platform == RuntimePlatform.IPhonePlayer) {
#if UNITY_IOS
			shareImage(imagePath, text);
#endif
        } else { 
            Debug.Log("No sharing set up for this platform.");
        }
    }

#if UNITY_IOS
	[DllImport("__Internal")]
	private static extern void shareImage(string imagePath, string text);

	[DllImport("__Internal")]
	private static extern void shareText(string text, string url);
#endif

}
